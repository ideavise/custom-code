Ext.define('Practice.view.main.Actions', {
    extend: 'Ext.app.Controller',
    
    alias: 'custom',
//removeaction property on grid - set to name of custom action
//addaction property on grid - set to name of custom function    
    /* -----------------------------------------------------------------------
    Go Previous Dashboard:
    Finds the last dashboard visited by the user and opens it
    __________________________________________________________________________ */
    
    gopreviousdashboardHandler: function (sp) {
        var arr = this.main.render.getHistory();
     	
	this.main.cache.invalidate(this.main.entity.getUser());
        if (arr.length>0){
            for (var i=arr.length-1,l=0;i>l;i--){
		var item = arr[i];
                var src = item.source||item.redirectsource;
                if (/dashboard\b/.test(String(src))) {
                    this.main.actions.perform(item);
                    break;
                }
            }
        }
    },
    
    gobackHandler: function (sp) {
        var arr = this.main.render.getHistory();
        var last = arr.pop();
        var item = arr.pop();
        this.main.actions.perform(item);
    },
    
    triggerrequiredHandler: function (sp) {
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
    onlinestatusHandler: function (sp) {

        var btn = this.main.view.down('*[n="onlineindicator"]');

        var fl = this.main.environment.getOnline();
        if (fl) {
            btn.addCls('online');
            btn.removeCls('offline');
        } else {
            btn.addCls('offline');
            btn.removeCls('online');
        }
    },

    clearcacheHandler: function (sp) {
	window.cache.clear(this.clearcachesuccessCallback,this.clearcacheerrorCallback);	
    },

    clearcachesuccessCallback: function (status) {
	alert('Cache cleared: ' + status);
    },

    clearcacheerrorCallback: function (status) {
	alert('Error: ' + status);
    },

    /* -----------------------------------------------------------------------
     * loadpreviousdou
     * Loads the athlete data for the test sessions in the grid.  This should
     * pull in the previous declarations in case the DCO goes offline
     * ____________________________________________________________________*/

    loadpreviousdouHandler: function (sp) {
	var me = (sp.scope||this);
	var data = sp.data;
	for (var i=0,l=data.length;i<l;i++) {
		var ath = this.main.data.filter(data[i].children,'cid EQ athlete');
		if (Ext.isObject(ath[0])) {
			this.main.actions.perform({
            		    cmd: 'request',
            		    a: ath[0].ca,
            		    relationship: 'children',
            		    callback: Ext.emptyFn,
            		    scope: this,
            		    component: sp.cmp
            		});
		}
	}
    },

    
     /*--------------------------------------------------------------------------
     * reset IC training enviroment
     * Removes the training events and adds them again 
     * ________________________________________________________________________*/

    resetictrainingenvironmentHandler: function (sp) {
	sp.component.setDisabled(true);

	// Get all of the users data
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
              this.main.actions.perform({
	      	     cmd: 'request',
	      	     a: this.main.entity.getUser().ca,
	      	     relationship: 'descendants',
	      	     callback: this.removetrainingeventsCallback,
		     failure: this.reseteventsfailureCallback,
	      	     scope: this,
	      	     component: sp
	      });
	} else {
		Ext.callback(this.removetrainingeventsCallback,this,[{
			desc: this.main.data.getDescendants(this.main.entity.getUser()),
			scope: this,
			component: sp
		}]);
	}
    },

    reseteventsfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);

	    rsp.component.component.setDisabled(false);
    },

    removetrainingeventsCallback: function (rsp) {
        var me = (rsp.scope||this);
	var desc = rsp.desc;

	if (Ext.isDefined(desc)){
		// Remove the events
		var evt = me.main.data.filter(desc, 'cid EQ event');
		evt.forEach(function(item, index, array) {
			if (/TRAINING/.test(String(item.l))){
	        		me.main.data.move({
	        		    component: rsp.component,
	        		    scope: me,
	        		    source: item,
	        		    destination: me.main.environment.get("RECYCLEBIN")
	        		});
				console.log('moved item ' + index);
	       		}
		});
	}
	
	// Load the training events
	if (me.main.environment.getOnline()&&!me.main.environment.get('safemode')){
		me.main.actions.perform({
		      cmd: 'request',
		      a: 'folder.7.2017.7.19.20.41.49.259',
		      relationship: 'descendants',
		      callback: me.addtrainingeventsCallback,
		      failure: me.addeventsfailureCallback,
		      scope: me,
		      component: rsp.component
		});
	} else {
		Ext.callback(me.addtrainingeventsCallback,me,[{
			desc: me.main.data.getDescendants(me.main.data.getObjectByName('trainingcontent')),
			scope: me,
			component: rsp.component
		}]);
	}

    },

    addeventsfailureCallback: function (rsp) {
	    var me=(rsp.scope||this);
	    rsp.component.component.setDisabled(false);
    },

    addtrainingeventsCallback: function (rsp) {
	var me = (rsp.scope||this);
	var desc = rsp.desc;
	
	if (Ext.isDefined(desc)) {
		var evt = me.main.data.filter(desc, 'cid EQ event');
		evt.forEach(function (item, index, array){
			me.main.data.createLink({
				parent: me.main.entity.getUser(),
				child: item
			});
		});
	}

	rsp.component.component.setDisabled(false);

	me.main.feedback.say({
            title: 'Reset Completed',
            message:'Paperless Training Events have been reset.'
        });


    },

     /*--------------------------------------------------------------------------
     * reset OOC training enviroment
     * Removes the training tests and associated documents 
     * ________________________________________________________________________*/

    resetooctrainingenvironmentHandler: function (sp) {
	
	// Get all of the users data
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
        	this.main.actions.perform({
        	    cmd: 'setbusy',
        	    component: this.main.view.down("*[name='trainingform']"), 
        	    message: 'Resetting training tests...'
        	});      
		
		this.main.actions.perform({
	      	     cmd: 'request',
	      	     a: this.main.entity.getUser().ca,
	      	     relationship: 'descendants',
	      	     callback: this.removetrainingtestsCallback,
		     failure: this.removetestsfailureCallback,
	      	     scope: this,
	      	     component: sp
	      });
	} else {
		this.main.feedback.say({
			title: 'Error',
			message: 'Paperless must be in online mode to reset the training environment'
		});
		return;
	}
    },

    removetestsfailureCallback: function (rsp) {
	var me = (rsp.scope||this);
	me.main.actions.perform({
            cmd: 'removebusy',
            component: me.main.view.down("*[name='trainingform']")
        });
    },

    removetrainingtestsCallback: function (rsp) {
        var me = (rsp.scope||this);
	var desc = rsp.desc;

	if (Ext.isDefined(desc)){
		// remove test sessions
		var tts = me.main.data.filter(desc, 'cid EQ testsession');
		tts.forEach(function(item, index, array){
			if (/TRAINING/.test(String(item.l))){
	        		me.main.data.move({
	        		    component: rsp.component,
	        		    scope: me,
	        		    source: item,
	        		    destination: me.main.environment.get("RECYCLEBIN")
	        		});
	       		}	
		});
		
		// remove incomplete manifests
		var tmf = me.main.data.filter(desc, 'cid EQ samplemanifest');
		tmf.forEach(function(item, index, array){
			if (item.t=='0'){
				me.main.data.move({
		    			component: rsp.component,
		    			scope: me,
		    			source: item,
		    			destination: me.main.environment.get("RECYCLEBIN")
	    			});
			}
		});;

	}

    	me.addtrainingtestsHandler(rsp);

    },


    /* ------------------------------------------------------------------------
     * add training tests
     * create test sessions for use in Paperless training.
     * ______________________________________________________________________*/

    addtrainingtestsHandler: function (rsp) {

        var me = (rsp.scope||this);
		
	var cb = Ext.bind(me.createtrainingtestsCallback,me);

	if (me.main.environment.getOnline()&&!me.main.environment.get('safemode')){
		me.main.actions.perform({
			cmd: 'request',
			a: 'folder.7.2017.7.19.20.41.49.259', // training content folder
			relationship: 'descendants',
			failure: me.createtestsfailureCallback,
			callback: cb,
			scope: me,
			component: rsp.component
		});
	} else {
		me.main.actions.perform({
            		cmd: 'removebusy',
            		component: me.main.view.down("*[name='trainingform']")
        	});
		me.main.feedback.say({
			title: 'Error',
			message: 'Paperless must be online to reset the training environment.'
		});
		return;
	}

    },
    
    createtestsfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);
	    me.main.actions.perform({
            	cmd: 'removebusy',
            	component: me.main.view.down("*[name='trainingform']")
            });

    },

    createtrainingtestsCallback: function (rsp) {

        var me = (rsp.scope||this);

	var desc = rsp.desc;
	var atharr = me.main.data.filter(desc,'cid EQ athlete');

	// Create an array of random screens to assign to the tests
	var sarr = me.buildrandomscreenarray();
	var testsprocessed = 0;
	var done = Ext.bind(me.createtrainingtestscomplete,rsp.scope,[rsp,rsp.component],1);

	atharr.forEach(function(item,index,array) {

		if (/TRAININGAthlete/.test(String(item.n))){
			// Lookup the id values to write to the test session
        		var sportval = item.Sport || '';
        		var sportId = '';
        		if (sportval) sportId = me.main.localization.reverselookupValue('sport','Sport',sportval,'SportId');
        		
        		var disciplineval = item.Discipline || '';
        		var disciplineId = '';
        		if (disciplineval) disciplineId = me.main.localization.reverselookupValue('discipline','Discipline',disciplineval,'DisciplineId');
        
			var lbl = 'TRAINING';
			// Set the start date to three days before today
			var startdt = new Date();
			startdt.setDate(startdt.getDate() -3 );
			// Set the end date to just over two weeks from today
			var enddt = new Date();
			enddt.setDate(enddt.getDate() + 15);

			var screens = sarr[index];
			// link the screens to the callback
       			var cb = Ext.bind(me.linktrainingathletesCallback,me,[item,screens],1);
			me.main.data.create({
       			    callback: cb,
       			    component: rsp.component,
       			    scope: me,
       			    data: [{
       			        cid: 'testsession',
       			        l: lbl.concat(index+1) ,
       			        pa:me.main.entity.getUser().ca, 
       			        t: 0,
       	 			TSID: lbl.concat(index+1),
       	 			AttemptType: 'National',
       	 			SiteID: 'OOC',
       	 			EventType: 'OOC',
       	 			designation: 'nonevent',
       			        Status: 'ASSIGNED',
       			        TestingAuthority: 'USADA',
       			        ResultsManagementAuthority: 'USADA',
       	 			TestingAuthorityLongName:  'United States Anti-Doping Agency',
       	 			ResultsAuthorityLongName: 'United States Anti-Doping Agency',
       	 			SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
       	 			CollectionAuthority: 'USADA',
       	 			TestClientLongName:  'United States Anti-Doping Agency',
       	 			TestClientShortName:  'USADA',
       	 			TestSessionAssignmentStatusType: 'Accepted',
       	 			TSStartDt: me.main.util.dateToISO8601(startdt),
       	 			TSEndDt: me.main.util.dateToISO8601(enddt), 
       	 			Sport: sportId,
       	     			Sport_caption: sportval,
       	     			Discipline: disciplineId,
       	     			Discipline_caption: disciplineval,
       	     			AddressType: item.AddressType||'',
       	     			ResidenceAddr1: item.Addr1||'',
       	     			ResidenceCity: item.City||'',
       	     			ResidenceState: item.State||'',
       	     			ResidenceZip: item.Zip||'',
       	     			ResidenceCountry_caption: item.Country||'',
       	     			ResidenceCountry: item.CountryId
       	    		}]
       			});
		}

		testsprocessed++;
		if (testsprocessed===array.length) done();
	});
	
    },


    linktrainingathletesCallback: function (rsp,aa,screens) {

        var me = (rsp.scope||this);
	var ts = rsp.objects[0];

	// Link the athlete to the new test session record
       	me.main.data.createLink({
       	    parent: ts,
       	    child: aa
       	});

	// Create the screens for this test session
	screens.forEach(function(item,index,array) {
		var lbl = 'labscreen.';
		var cb = Ext.bind(me.marktrainingtestsamplesCallback,me,[rsp,ts],1);
		var tsid = ts.TSID;
		tsid = tsid.concat('.0');
		me.main.data.create({
			callback: cb,
			component: rsp.component,
			scope: me,
			data: [{
				cid: 'labscreen',
				pa: ts.a,
				t: 0,
				l: lbl.concat(tsid,'.',item.ScreenTypeId),
				ScreenTypeId: item.ScreenTypeId,
				DefaultTestMethod: item.DefaultTestMethod,
				LabId: item.LabId,
				TestSessionAttemptId: tsid,
				TestSessionAttemptScreenId: tsid.concat('.',item.ScreenTypeId),
				TestSessionId: ts.TSID,
				ix: index
			}]
		});
 	   });

    },

    marktrainingtestsamplesCallback: function (rsp,ts) {
       	
	var me = (rsp.scope||this);	
	var test = ts.objects[0];
	var desc = me.main.data.getChildren(test);
	var scrns = me.main.data.filter(desc,'cid EQ labscreen');
	var isurine = false;
	var isblood = false;
	var isdbs = false;

	scrns.forEach(function(item,index,array){
		if (/Urine/.test(String(item.DefaultTestMethod))) {
			isurine = true;	
		} else if (/DBS/.test(String(item.DefaultTestMethod))) {
			isdbs = true;
        	} else if (/Blood\b/.test(String(item.DefaultTestMethod))) {
			isblood = true;
		}
	});
        	
       	// Add athlete info to test session if it exists.
       	me.main.actions.perform({
       	    cmd: "put",
       	    component: rsp.component,
       	    objects: [test],
       	    Urine: isurine,
	    UrineType: 'partial',
	    Blood: isblood,
	    DBS: isdbs
       	});
     
    },

    createtrainingtestscomplete: function (rsp,cmp) { 
   	var me = (rsp.scope||this); 

	me.main.actions.perform({
            cmd: 'removebusy',
            component: me.main.view.down("*[name='trainingform']")
        });
	
	me.main.feedback.say({
                title: 'Reset Completed',
                message:'Paperless Training OOC test sessions have been reset.'
            });
    },

    buildrandomscreenarray: function () {
    	// Urine screens actively used by USADA
	var uscreens = [
			{
				ScreenTypeId: '2',
				DefaultTestMethod: 'Urine',
				LabId:'2'
			},
			{
				ScreenTypeId: '5',
				DefaultTestMethod: 'Urine',
				LabId: '2'
			},
			{
				ScreenTypeId: '3',
				DefaultTestMethod: 'Urine',
				LabId: '2'
			},
			{	ScreenTypeId: '18',
				DefaultTestMethod: 'Urine',
				LabId: '2'
			}
	];

	// Blood screens actively used by USADA
	var bscreens = [
			{
				ScreenTypeId: '8',
				DefaultTestMethod: 'Blood-EDTA',
				LabId: '2'
			},
			{
				ScreenTypeId: '12',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			},
			{
				ScreenTypeId: '17',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			},
			{
				ScreenTypeId: '15',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			},
			{
				ScreenTypeId: '4',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			},
			{
				ScreenTypeId: '11',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			},
			{
				ScreenTypeId: '7',
				DefaultTestMethod: 'Blood-Serum',
				LabId: '2'
			}
	];

	// DBS screens actvely used by USADA
	var dscreens = [
			{
				ScreenTypeId: '29',
				DefaultTestMethod: 'DBS',
				LabId: '2'
			},
			{
				ScreenTypeId: '30',
				DefaultTestMethod: 'DBS',
				LabId: '2'
			},
			{
				ScreenTypeId: '31',
				DefaultTestMethod: 'DBS',
				LabId: '2'
			}
	];

	/* Build a random array of screens for the test sessions
	 * need following combinations for 10 tests:
         * 2 urine
         * 2 blood
         * 1 DBS
         * 2 urine and blood
         * 1 urine, blood, DBS
         * 1 urine DBS
         * 1 blood DBS
	 * _________________________________________*/

	var screenarray = [];
	screenarray.push([uscreens[0]]);
	screenarray.push([uscreens[0],uscreens[Math.floor((Math.random() * 3) + 1)],bscreens[Math.floor(Math.random() * 7)]]);
	screenarray.push([dscreens[Math.floor(Math.random() * 3)]]);
	screenarray.push([bscreens[Math.floor(Math.random() * 7)],dscreens[Math.floor(Math.random() * 3)]]);
	screenarray.push([bscreens[Math.floor(Math.random() * 7)]]);
	screenarray.push([uscreens[0],bscreens[Math.floor(Math.random() * 7)],dscreens[Math.floor(Math.random() * 3)]]);
	screenarray.push([uscreens[0],bscreens[Math.floor(Math.random() * 7)]]);
	screenarray.push([bscreens[Math.floor(Math.random() * 7)]]);
	screenarray.push([uscreens[0],uscreens[Math.floor((Math.random() * 3) + 1)],dscreens[Math.floor(Math.random() * 3)]]);
	screenarray.push([uscreens[0],uscreens[Math.floor((Math.random() * 3) + 1)]]);

	return screenarray;
	
    },
       
    
    /* -----------------------------------------------------------------------
    Start DCF:
    Opens the DCF form for the selected test session
    __________________________________________________________________________ */
    
    startdcfHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
     
	// Pre-populate the DCO name in the final signatures section
	if (!Ext.isDefined(ats.DCOPNFinal)) {
		this.main.actions.perform({
			cmd: "put",
			component: this,
			objects:[ats],
			DCOPNFinal: this.main.entity.getUser().PersonId,
				DCOPNFinal_caption: this.main.entity.getUser().FirstName + ' ' + this.main.entity.getUser().LastName
		});
	}
    
        // Enable/disable blood form
        var bforms = this.main.data.getObjectByName('dcorbl');
        var disablebl = (ats.Blood===true) ? false : true;
        bforms.disabled = disablebl;
        
	// Enable/disable DBS form
	var dbsforms = this.main.data.getObjectByName('dcordbs');
	var disabledbs = (ats.DBS===true) ? false : true;
	dbsforms.disabled = disabledbs;

        // Enable/disable urine form
        var uforms = this.main.data.getObjectByName('dcorur');
        var disableur = (ats.Urine===true) ? false : true;
        uforms.disabled = disableur;
           
	// Set the urine test type
	if (!Ext.isDefined(ats.UrineType)) {
		var utype = 'none';
		// For OOC tests set the urine type based on screens
		if (ats.designation=='nonevent'){
			var uscreens = this.main.data.filter(this.main.data.getDescendants(ats),'cid EQ labscreen AND DefaultTestMethod EQ Urine');
			uscreens.forEach(function(item,index,array) {
				if (item.ScreenTypeId==2) {
					utype = 'partial';
				} else if (item.ScreenTypeId==1) {
					utype = 'full';
				}
			});
		} else {
			// IC test, set urine type to full
			utype = 'full';
		}
		this.main.actions.perform({
			cmd: 'put',
			component: this,
			objects:[ats],
			UrineType: utype
		});
	}

        // Sets time zone to the current context
        // every time test session is opened
        this.main.actions.perform({
             cmd: "put",
             component: this,
             objects: [ats],
             TimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
        });
        
        // If necessary, set SiteId to OOC for out of competition tests
        if (!Ext.isDefined(ats.SiteID)){
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                SiteID: 'OOC'
            });
        }
	// Save the designation so that the appropriate previous declarations can be loaded.
	var designation = (ats.designation=='nonevent') ? 'OOC' : 'IC';
        this.main.environment.set("doudesignation",designation);
	this.main.data.setProvider("{activeathlete}",this.main.data.filter(this.main.data.getChildren(ats),'cid EQ athlete')[0]);
	
	this.main.server.setSafeMode(true,'true');
	this.main.actions.perform({
        	cmd: 'goto',
        	redirectsource: 'dcfForm',
        	target: 'userinterface'
        });

       	//var desc = this.main.data.getDescendants(ats.a);
        //var arr = this.main.data.filter(desc,'cid EQ athlete');

	//var cb = Ext.bind(this.opendcfCallback,this);
	//var cb2 = Ext.bind(this.offlineopendcfCallback,this);
        //
	//if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
        // 	this.main.actions.perform({
        //    	    cmd: 'request',
        //    	    a: arr[0].ca,
        //    	    relationship: 'children',
        //    	    callback: cb,
        //    	    scope: this,
        //    	    component: sp.component
        //    	});
	//} else {
	//   	Ext.callback(cb2,this,[{
	//	   //leaf: {children: this.main.data.getChildren(this.main.data.getSource(arr[0]).ca)},
	//           leaf: this.main.data.getSource(arr[0]),
	//           scope: this,
	//           component: sp.component
	//  	 }]);
	//}
    },

    offlineopendcfCallback: function (rsp) {

        var me = (rsp.scope||this);
	var dous = me.main.data.getChildren(rsp.leaf.ca);
	var ath = Ext.apply({children: dous},rsp.leaf);

    
	var cb = Ext.bind(me.opendcfCallback,me);
	Ext.callback(cb,me,[{
		leaf: ath, 
		scope: me,
		component: rsp.component
	}]);
    },

    opendcfCallback: function (rsp) {

        var me = (rsp.scope||this);
	if (Ext.isObject(rsp.leaf)){
  		me.main.data.setProvider("{activeathlete}",rsp.leaf); 
		me.main.server.setSafeMode(true,'true');

        	       
        	me.main.actions.perform({
        	    cmd: 'goto',
        	    redirectsource: 'dcfForm',
        	    target: 'userinterface'
        	});
	} else {
	    me.main.feedback.say({
                title: 'Error',
                message:'Cannot find athlete associated with this test.  Switch Paperless to online mode and try again.'
            })
	}

    
    },
    
    /* -----------------------------------------------------------------------
    Toggle Blood forms:
    Enable/Disable blood forms when the Blood checkbox is toggled
    __________________________________________________________________________ */
    
    togglebloodformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var btab = this.main.util.get('sessionmanager').tabBar.items.items[4];
        var bforms = this.main.util.get('dcorbl');
        if (ats.Blood===true) {
            bforms.enable(true);
            btab.enable(true);
        } else {
            bforms.disable(true);
            btab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Toggle DBS forms:
    Enable/Disable dbs forms when the DBS checkbox is toggled
    __________________________________________________________________________ */
    
    toggledbsformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var dbstab = this.main.util.get('sessionmanager').tabBar.items.items[5];
        var dbsforms = this.main.util.get('dcordbs');
        if (ats.DBS===true) {
            dbsforms.enable(true);
            dbstab.enable(true);
        } else {
            dbsforms.disable(true);
            dbstab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },

    /* -----------------------------------------------------------------------
    Toggle Urine forms:
    Enable/Disable urine forms when the Urine checkbox is toggled
    __________________________________________________________________________ */
    
    toggleurineformsHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        var utab = this.main.util.get('sessionmanager').tabBar.items.items[6];
        var uforms = this.main.util.get('dcorur');
        if (ats.Urine===true) {
            uforms.enable(true);
            utab.enable(true);
        } else {
            uforms.disable(true);
            utab.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
//        this.main.rules.broadcastTopic({topic:'refresh',component:sm});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    refreshdeclarationslistHandler: function (sp) {
        var deccmp = this.main.util.get('declarations');
        if (deccmp) deccmp.refresh();
    },
    
    /* -----------------------------------------------------------------------
     * Toggle precomp
     * Modifies test settings if pre-comp test
     * ____________________________________________________________________*/

    toggleprecompHandler: function (sp) {
	var ats = this.main.data.getProvider("activetestsession");
	// If precomp is selected then reset as a blood test
	// and set test type to OOC
	if (sp.component.value) {
		this.main.actions.perform({
			cmd: 'put',
			component: this,
			objects:[ats],
			Urine: false,
			Blood: true,
			EventType: 'OOC',
			UrineType: 'none'
		});
		var utab = this.main.util.get('sessionmanager').tabBar.items.items[6];
        	var uforms = this.main.util.get('dcorur');
        	uforms.disable(true);
        	utab.disable(true);
		var btab = this.main.util.get('sessionmanager').tabBar.items.items[4];
        	var bforms = this.main.util.get('dcorbl');
        	bforms.enable(true);
        	btab.enable(true);
       	}

	this.main.rules.broadcastTopic({topic:'{activetestsession}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});

    },

    /* -----------------------------------------------------------------------
    Incomplete Test:
    Allows user to sumbit test without completing required fields
    __________________________________________________________________________ */
    
    incompletetestHandler: function (sp) {
        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
    /* -----------------------------------------------------------------------
    Copy Notification Date:
    Copies the notification date from the notification form to the athlete info
    form.
    __________________________________________________________________________ */
    
    copynotificationdateHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
        
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            ArrivalDate: ats.NotificationDate,
            ArrivalTime: ats.NotificationTime
        });
        
        var adate = this.main.util.get('ArrivalDate');
        var atime = this.main.util.get('ArrivalTime');
        
        adate.setValue(ats.NotificationDate);
        adate.commitValue();
        
        //atime.setValue(ats.NotificationTime);
        //atime.commitValue(atime,ats.NotificationTime,null);

        this.main.rules.broadcastTopic({topic:'{activetestsession}'});
        this.main.rules.broadcastTopic({topic:'requiredchange'});

    },
    
    /* -----------------------------------------------------------------------
    Update test session status:
    Update the status of the test to OPENED after the athlete signs the 
    notification form.  TODO: how/when to call this?
    __________________________________________________________________________ */
    
    updatetestsessionstatusHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
                // Change status of test session once the athlete has signed notification
        if (ats.notificationPart4_completed && this.main.data.getDerivedValue(ats, '_Status')=='ASSIGNED'){
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                _Status: 'OPENED'
            });
        }
    },
    
    /* -----------------------------------------------------------------------
    Copy Address:
    Copies the values in the Residence Address form to the matching fields
    in the Mailing Address form.
    __________________________________________________________________________ */
    
    copyaddressHandler: function (sp) {
//        debugger;
        
        if (sp.component.value==true) {
            var ats = this.main.data.getProvider("activetestsession");
           
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [ats],
                MailingAddr1: ats.ResidenceAddr1,
                MailingCity: ats.ResidenceCity,
                MailingState: ats.ResidenceState,
                MailingZip: ats.ResidenceZip,
                MailingCountry: ats.ResidenceCountry
            });
           
           // If first field in the mailing address set is triggered
           // the rest of the fields are populated automatically from
           // the values in the active test session
            this.main.util.get('MailingAddr1').setValue(ats.ResidenceAddr1);
            this.main.util.get('MailingAddr1').commitValue();

            this.main.rules.broadcastTopic({topic:'{activetestsession}'});
            this.main.rules.broadcastTopic({topic:'requiredchange'});
           
        }
        
    },
    
    doLoadActiveDocumentHandler: function (sp) {
        var ad = this.main.data.getProvider("activedocument");
    //TODO Sample Manifest document isn't loading after app is reloaded

        var doccmp = this.main.data.getObjectByName('DocumentReviewer');
        doccmp.ct = ad.ct;
        
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'docreviewpanel',
            target: 'userinterface'
        });
    },

     /* -------------------------------------------------------------------
     * Blood sample barcode scanner
     * _________________________________________________________________*/

    scanbsampleHandler: function (sp) {
	var samplefld = this.main.util.get('BloodSampleCode');
	samplefld.setValue(sp.value);
        samplefld.commitValue();
    },

    /* -------------------------------------------------------------------
     * DBS sample barcode scanner
     * _________________________________________________________________*/

    scandbssampleHandler: function (sp) {
	var samplefld = this.main.util.get('DBSSampleCode');
	samplefld.setValue(sp.value);
        samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Urine sample barcode scanner
     * _________________________________________________________________*/

    scanusampleHandler: function (sp) {
	    var samplefld = this.main.util.get('UrineSampleCode');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Tracking number barcode scanner
     * _________________________________________________________________*/

    scantrackingnumberHandler: function (sp) {
	    var samplefld = this.main.util.get('TrackingNumber');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

     /* -------------------------------------------------------------------
     * Monitor number barcode scanner
     * _________________________________________________________________*/

    scanmonitornumberHandler: function (sp) {
	    var samplefld = this.main.util.get('MonitorNumber');
	    samplefld.setValue(sp.value);
	    samplefld.commitValue();
    },

    // Trigger business logic when a new blood sample is added
    addbloodsampleHandler: function (sp){
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'{activebloodsample}'});
		this.main.rules.broadcastTopic({topic:'requiredchange}'});
	    },
	    1200,this);
    },

    removebloodsampleHandler: function (sp) {
	Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'{activebloodsample}'});
		this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200, this);
    },

    //Trigger business logic when a new dried blood spot is added
    adddbssampleHandler: function(sp) {
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'{activedbssample}'});
		this.main.rules.broadcastTopic({topic:'requiredchange'});
	    },
	    1200, this);
    },

    removedbssampleHandler: function(sp) {
	 Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'{activedbssample}'});
		this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200, this);
    },
   

    /* -------------------------------------------------------------------
     * Functions to handle the Urine Sample form multiples
     * _________________________________________________________________*/

    editurinesampleHandler: function (sp) {
	// Run the buisness logic for the selected Urine sample
	this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});

        var samplelist = this.main.util.get('urinetabs');
        //samplelist.collapse();
	samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
    
    addurinesampleHandler: function (sp) {
	// Run the business logic to include new urine sample
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	    this.checksubsequentrequiredHandler(sp);
	},
	1200,
	this)
    },

    removeurinesampleHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activeurinesample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	    this.checksubsequentrequiredHandler(sp);
	},
	1200,
	this)
    },

    checksubsequentrequiredHandler: function (sp) {
	// Sets subsequentofficerrequired flag to true in the test session
	// if more than one urine sample is found.  Will enable/disable the
	// Subsequent Officer tab in the signatures section
	var adp = this.main.data.getProvider("{activetestsession}");
	var desc = this.main.data.getDescendants(adp.a);
        var arr = this.main.data.filter(desc,'cid EQ urinesample');

	var v = (arr.length>1) ? true : false;
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [adp],
            subsequentofficerrequired: v
        });

	this.main.rules.broadcastTopic({topic:'subsequentofficerrequired'});
    },

    /* -------------------------------------------------------------------
     * Functions to handle the Partial Sample  form multiples
     * _________________________________________________________________*/
   
    editpartialsampleHandler: function (sp) {
	// Run the business logic for the selected Partial Sample
	this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});

	// Hide the navigation grid
        var samplelist = this.main.util.get('ptdetailstabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
    
    addpartialsampleHandler: function (sp) {
	// Run the business logic to include the new Partial Sample
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    removepartialsampleHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activepartialsample}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },

    /* -------------------------------------------------------------------
     * Functions to handle the Custody transfer form multiples
     * _________________________________________________________________*/
    
    editcustodytransferHandler: function (sp) {
	// Run the business logic for the selected Custody Transfer
	this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
    	this.main.rules.broadcastTopic({topic:'requiredchange'});

	// Hide the navigation grid
        var samplelist = this.main.util.get('custodytabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },

    addcustodytransferHandler: function (sp) {
	// Run the business logic to include the new Custody Transfer
	Ext.defer(function () {
	   this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
	   this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },

    removecustodytransferHandler: function (sp) {
	// Run the business logic for the updated list
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'{activecustodytransfer}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
	},
	1200,
	this)
    },
    
    /* -------------------------------------------------------------------
     * Functions to handle the Supplementary Report  form multiples
     * _________________________________________________________________*/
    
    addsuppreportHandler: function (sp) {
	    Ext.defer(function () {
        	this.main.data.setProvider("{supplementarysupportperson}",{});
		// force business logic to update
		this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
        	this.main.rules.broadcastTopic({topic:'requiredchange'});
                this.main.util.get('addsuppsignaturesgrid').loadData();
	    },
	    1200,
	    this)
    },

    removesuppreportHandler: function (sp) {
       Ext.defer(function () {
           this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
           this.main.rules.broadcastTopic({topic:'requiredchange'});
        },
        1200,
        this)
    },

    editpresuppHandler: function (sp) {
	// Configure the provider for the Signature form
	// this is a special case because we have a multiple (signatures) within
	// a multiple (supplementary forms)
	var sup = this.main.data.getProvider("activesupplementary");
	var supsigs = this.main.data.filter(this.main.data.getChildren(sup),"cid EQ supportperson");
	this.main.data.setProvider("{supplementarysupportperson}",supsigs);

        var suppgrd = this.main.util.get('addsuppsignaturesgrid');
	suppgrd.loadData();
	// Select the first item in the grid
	//var grd = suppgrd.down("grid");
	//var rec = grd.getStore().config.data[0];
        //var store = grid.getStore();
        //var data = store.proxy.data;
	//var rec = grd.store.first();
	//grd.getSelectionModel().select([rec]);
	//grd.setSelection(0);
	
	this.main.rules.broadcastTopic({topic:'{activesupplementary}'});
	this.main.rules.broadcastTopic({topic:'requiredchange'});
	this.main.rules.broadcastTopic({topic:'{supplementarysupportperson}'});


        var samplelist = this.main.util.get('suptabs');
        samplelist.setCollapsed(false);
	samplelist.setActiveTab(0);
    },
   
    /* -----------------------------------------------------------------------
    startuar:
    Starts an Unsuccessful Attempt Report (UAR) for the test session:  If a UAR
    already exists for the test session it is used again, otherwise a new UAR
    document is created; Starts providers for the two locations in the UAR.
    __________________________________________________________________________ */
    
    startuarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");
        
        // Reset all providers
//        this.main.data.setProvider("uarphonecalls",{});
//        this.main.data.setProvider("uarsecondphonecalls",{});

        // Mark the test session as opened
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [par],
            _Status: 'OPENED'
        });

        // Find if the test session already contains a UAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ uar');
        
        if (arr.length > 0){
            this.startuarCallback(arr);
        } else {
            var aarr = this.main.data.filter(desc,'cid EQ athlete');
            var apn = '';
            var spt = ''
            if (aarr.length>0) {
                if (Ext.isDefined(aarr[0].FirstName) && Ext.isDefined(aarr[0].LastName)) apn = apn.concat(aarr[0].FirstName,' ',aarr[0].LastName);
                if (Ext.isDefined(aarr[0].Sport)) spt = aarr[0].Sport;
           }
           
        
            this.main.data.create({
                callback: this.startuarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'uar',
                    l:  'Unsuccessful Attempt',
                    pa: par.ca,
                    t: 0,
                    AthletePrintedName: apn,
                    Sport: spt,
                    TestingAuthority: par.TestingAuthority,
                    AttemptType: par.AttemptType
                }]
            });
        }
        
    },
    
    startuarCallback: function (rsp) {
    
        // set the uar to the activeuar provider and open the uar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var uar, loc1, loc2;
        if (rsp.objects) {
            // A new UAR object was created
            uar = rsp.objects[0];
           
        } else {
            // UAR already exists
            uar = rsp[0];
        }
        
        me.main.data.setProvider("{activeuar}",uar.a);
        
        // enable/disable first location phone calls
        var phonelist = me.main.data.getObjectByName('uarfirstnumberscalledgrid');

        if (uar.FirstCallsPlaced==true) {
           phonelist.disabled=false;
        } else {
           phonelist.disabled=true;
        }
        
        // enable/disable second location fields
        
        var loc = me.main.data.getObjectByName('secondlocfs');
        var time = me.main.data.getObjectByName('secondloctimefs');
        var contact = me.main.data.getObjectByName('secondlocpersoncontactedfs');
        var access = me.main.data.getObjectByName('secondlocrestrictedaccessfs');
        var phone = me.main.data.getObjectByName('secondloccallsplacedfs');
        var secondphonelist = me.main.data.getObjectByName('uarsecondnumberscalledgrid');
        var comments = me.main.data.getObjectByName('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.disabled=false;
            time.disabled=false;
            contact.disabled=false;
            access.disabled=false;
            phone.disabled=false;
            secondphonelist.disabled = (uar.SecondCallsPlaced==true) ? false : true;
            comments.disabled=false;
        } else {
            loc.disabled=true;
            time.disabled=true;
            contact.disabled=true;
            access.disabled=true;
            phone.disabled=true;
            secondphonelist.disabled=true;
            comments.disabled=true;
        }
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'uarpanel',
            target: 'userinterface',
            provider: 'activeuar'
        });
	
	me.main.server.setSafeMode(true,'true');

    },
    
    
    /* -----------------------------------------------------------------------
    addfirstloccallsHandler:
    Run business logic when a phone number is added or removed
    __________________________________________________________________________ */

    addfirstloccallsHandler: function (sp) {
	// disable Phone Calls Placed checkbox
        var auar = this.main.data.getProvider("activeuar");
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [auar],
            FirstLocPhoneExists: 'Yes'
        });
	// Run the business logic 
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'FirstLocPhoneExists'});
	    this.main.rules.broadcastTopic({topic:'{uarphonecalls}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    /*------------------------------------------------------------------------
     * deletefirstloccallsHandler:
     * Run business logic when phone number is removed
     * _____________________________________________________________________*/

    deletefirstloccallsHandler: function (sp) {
	    // see if we need to enable the Phone Calls Placed checkbox again
	    var uar = this.main.data.getProvider("activeuar");
            var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
	    var pfound = false;
            for (var i=0,l=pnumbers.length;i<l;i++) {
                if (pnumbers[i].UARLocation=='FirstLocation') pfound=true;
	    }

	    // If no numbers found for first location enable checkbox
	    if (pfound){
	    } else {
		    this.main.actions.perform({
			    cmd: "put",
			    component: this,
			    objects: [uar],
			    FirstLocPhoneExists: 'No'
		    });

        	    this.main.data.setProvider("{uarphonecalls}",{});
    	    }
            
            // Run the business logic
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'FirstLocPhoneExists'});
	    	this.main.rules.broadcastTopic({topic:'{uarphonecalls}'});
	    	this.main.rules.broadcastTopic({topic:'requiredchange'});
    	    },
    	    1200,
    	    this)    
    },


   
    /* -----------------------------------------------------------------------
    addsecondloccallsHandler:
    Run business logic when a phone number is added 
    __________________________________________________________________________ */

    addsecondloccallsHandler: function (sp) {
	// disable Phone Calls Placed checkbox
        var auar = this.main.data.getProvider("activeuar");
	this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [auar],
            SecondLocPhoneExists: 'Yes'
        });
	// Run the business logic 
	Ext.defer(function () {
	    this.main.rules.broadcastTopic({topic:'SecondLocPhoneExists'});
	    this.main.rules.broadcastTopic({topic:'{uarsecondphonecalls}'});
	    this.main.rules.broadcastTopic({topic:'requiredchange'});
    	},
    	1200,
    	this)
    },

    /*------------------------------------------------------------------------
     * deletesecondloccallsHandler:
     * Run business logic when phone number is removed
     * _____________________________________________________________________*/

    deletesecondloccallsHandler: function (sp) {
	    // see if we need to enable the Phone Calls Placed checkbox again
	    var uar = this.main.data.getProvider("activeuar");
            var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
	    var pfound = false;
            for (var i=0,l=pnumbers.length;i<l;i++) {
                if (pnumbers[i].UARLocation=='SecondLocation') pfound=true;
	    }

	    // If no numbers found for second location enable checkbox
	    if (pfound){
	    } else {
		    this.main.actions.perform({
			    cmd: "put",
			    component: this,
			    objects: [uar],
			    SecondLocPhoneExists: 'No'
		    });

        	    this.main.data.setProvider("{uarsecondphonecalls}",{});
    	    }
            
            // Run the business logic
	    Ext.defer(function () {
		this.main.rules.broadcastTopic({topic:'SecondLocPhoneExists'});
	    	this.main.rules.broadcastTopic({topic:'{uarsecondphonecalls}'});
	    	this.main.rules.broadcastTopic({topic:'requiredchange'});
    	    },
    	    1200,
    	    this)    
    },


    /* -----------------------------------------------------------------------
    secondlocvisited:
    Toggles disabled/enabled state of second location fields in uar
    __________________________________________________________________________ */
    
    secondlocvisitedHandler: function (sp) {
        var uar = this.main.data.getProvider("activeuar");
        var loc = this.main.util.get('secondlocfs');
        var time = this.main.util.get('secondloctimefs');
        var contact = this.main.util.get('secondlocpersoncontactedfs');
        var access = this.main.util.get('secondlocrestrictedaccessfs');
        var phone = this.main.util.get('secondloccallsplacedfs');
        var phonelist = this.main.util.get('uarsecondnumberscalledgrid');
        var phonedetails = this.main.util.get('uarsecondphonedetailsfs');
        var comments = this.main.util.get('secondloccommentsfs');
        
        if (uar.secondlocationvisited=="Yes") {
            loc.enable(true);
            time.enable(true);
            contact.enable(true);
            access.enable(true);
            phone.enable(true);
            comments.enable(true);
            if (uar.SecondCallsPlaced==true) {
                phonelist.enable(true);
                phonedetails.enable(true);
            }
        } else {
            loc.disable(true);
            time.disable(true);
            contact.disable(true);
            access.disable(true);
            phone.disable(true);
            phonelist.disable(true);
            phonedetails.disable(true);
            comments.disable(true);
        }
        
        
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
        
    },
    
    /* -----------------------------------------------------------------------
    submituar:
    Submits an Unsuccessful Attempt Report (UAR): creates UAR document; check
    that times are valid; display confirmation message; update status of 
    associated test session
    __________________________________________________________________________ */
    
    submituarHandler: function (sp) {
        var auar = this.main.data.getProvider("activeuar");
        
        // check that the times entered by user are valid
        var msgarr = [];
        var timemsg = '<div>You must correct the following time values:</div>';
        var timepass = false;
        if (auar.FirstArrivalTime>auar.FirstAttemptStart) msgarr.push('First Location: Arrival Time is later than Attempt Start');
        if (auar.FirstArrivalTime>auar.FirstAttemptEnd) msgarr.push('First Location: Arrival Time is later than Attempt End');
        if (auar.FirstAttemptStart>auar.FirstAttemptEnd) msgarr.push('First Location: Attempt Start is later than Attempt End');
        if (auar.SecondArrivalTime>auar.SecondAttemptStart) msgarr.push('Second Location: Arrival Time is later than Attempt Start');
        if (auar.SecondArrivalTime>auar.SecondAttemptEnd) msgarr.push('Second Location: Arrival Time is later than Attempt End');
        if (auar.SecondAttemptStart>auar.SecondAttemptEnd) msgarr.push('Second Location: Attempt Start is later than Attempt End');
        
        for (var i=0,l=msgarr.length;i<l;i++) {
           timemsg = timemsg + '<div>' + msgarr[i] + '</div>';
           timepass = true;
        }
        
        if (timepass) {
           this.main.feedback.say({
                title: 'Invalid Time Found',
                message: timemsg
            });
            return;
        }
        
        var cb = Ext.bind(this.submituarCallback,this);
        
        Ext.Msg.show({
            title: 'Submit Confirmation',
            message: 'All information, particularly the Comments Sections has been thoroughly reviewed and phrases such as “missed test” or “whereabouts failure” have been avoided, as well as ensuring all spelling and grammar is accurate?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: cb
        });
    },
    
    submituarCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        if (rsp==='no') {
            return;
        }
        
        var ts = new Date();
        var auar = me.main.data.getProvider("activeuar");
        var ats = me.main.data.getProvider("activetestsession");
        // Get the active uar and it's descendants
        var report = this.main.data.map[auar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(me.main.data.getObjectByName('UAROutput').ct,me.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        
        me.main.data.create({
            callback: me.submituarFeedback,
            component: me,
            scope: me,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'Unsuccessful Attempt Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'uar',
                ct: output,
                a: ats.a + '.uar'
            }]
        });
        
        // Update test session status
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [ats],
            _Status: 'COMPLETED',
            UAR_completed: true,
            DocumentGenerated: ts
        });
    },
    
    submituarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        //Ext.defer(function () {
        //    this.main.feedback.say({
        //        title: 'Success',
        //        message: 'Unsuccessful Attempt Report saved.'
        //    })
        //},
        //1200,
        //this)
    },

    
    /* -----------------------------------------------------------------------
    startaar:
    Starts an After Action Report (AAR) for the test session:  If a AAR
    already exists for the test session it is used again, otherwise a new AAR
    document is created; Starts providers for the sub-objects in the AAR.
    __________________________________________________________________________ */
    
    startaarHandler: function (sp) {
        
        var par = this.main.data.getProvider("activetestsession");

	if (!Ext.isDefined(par)){
		this.main.feedback.say({
			title: 'Error',
			message: 'Error starting After Action Report. Please try again'
		});
		return;
	}
        
        // Reset all providers
        //this.main.data.setProvider("activeaarlocation",{});
        
        // Find if the test session already contains an AAR
        var desc = this.main.data.getDescendants(par.a);
        var arr = this.main.data.filter(desc,'cid EQ aar');
        
        // Get the labs that the shipped samples were sent to and date sent
        var ucla, smrtl = false;
        var sdate;
        var uarr = this.main.data.filter(desc,'cid EQ urinesample');
        for (var i=0,l=uarr.length;i<l;i++) {
            if (uarr[i].SampleStatus=='SHIPPED'||uarr[i].SampleStatus=='SENT') {
                if (uarr[i].EPO=='1' || uarr[i].CIRIRMS=='1' || uarr[i].GHRP=='1') ucla=true;
                if (uarr[i].EPO=='2' || uarr[i].CIRIRMS=='2' || uarr[i].GHRP=='2') smrtl=true;
                sdate = uarr[i].DateShipped;
            }
        }
        
        var barr = this.main.data.filter(desc,'cid EQ bloodsample');
        for (var i=0,l=barr.length;i<l;i++) {
            if (barr[i].SampleStatus=='SHIPPED'||barr[i].SampleStatus=='SENT') {
                if (barr[i].Parameters=='1' || barr[i].HGH=='1' || barr[i].HGH2=='1' || barr[i].HBOC=='1' || barr[i].CERA=='1' || barr[i].HBT=='1' || barr[i].EPOBlood=='1') ucla=true;
                if (barr[i].Parameters=='2' || barr[i].HGH=='2' || barr[i].HGH2=='2' || barr[i].HBOC=='2' || barr[i].CERA=='2' || barr[i].HBT=='2' || barr[i].EPOBlood=='2') smrtl=true;
                sdate = barr[i].DateShipped;
            }
        }

	var dbsarr = this.main.data.filter(desc,'cid EQ dbssample');
	for (var i=0,l=dbsarr.length;i<l;i++) {
		if (dbsarr[i].SampleStatus=='SHIPPED'||dbsarr[i].SampleStatus=='SENT'){
			if (dbsarr[i].DBSPartial=='1' || dbsarr[i].DBSFull=='1' || dbsarr[i].DBSParameters=='1') ucla=true;
			if (dbsarr[i].DBSPartial=='2' || dbsarr[i].DBSFull=='2' || dbsarr[i].DBSParameters=='2') smrtl=true;
			sdate = dbsarr[i].DateShipped;
		}
	}
        
        if (arr.length > 0){
//            var aardesc = this.main.data.getDescendants(arr[0].ca);
//            var locs = this.main.data.filter(aardesc,'cid EQ location');
//            var comb = arr.concat(locs);
            //this.main.data.setProvider("activeaarlocation",{});
            this.startaarCallback(arr);
        } else {
            var atharr = this.main.data.filter(desc,'cid EQ athlete');
            var apn = '';
            apn = apn.concat(atharr[0].FirstName,' ',atharr[0].LastName);

            // Create AAR and pre-populate values from the completed test session
            this.main.data.create({
                callback: this.startaarCallback,
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'aar',
                    l:  'After Action Report - ' + apn,
                    pa: par.a,
                    t: 0,
                    LOCTArrivalTime: par.ArrivalTime,
                    LOCTNotificationTime: par.NotificationTime,
                    LOCTAttemptEnd: par.CompletionTime,
                    LOCTCity: par.EventCity || '',
                    LOCTState: par.EventState || '',
                    BCOPrintedName: par.BCOPrintedName || par.DBSBCOPrintedName || '',
                    AfterActionUCLA: ucla,
                    AfterActionSMRTL: smrtl,
                    AfterActionDateSent: sdate
                }]
            });
        }
        
    },

   startaarCallback: function (rsp) {
    
        // set the aar to the activeaar provider and open the aar form
        var me = (rsp.scope||this);
        var ts = me.main.data.getProvider("activetestsession");
        var aar, loc1;
        if (rsp.objects) {
            // A new AAR object was created
            aar = rsp.objects[0];
        } else {
            // AAR already exists
            aar = rsp[0];
        }
    
        me.main.data.setProvider("{activeaar}",aar.a);
        
        me.main.actions.perform({
            cmd: 'request',
            source: 'aarpanel',
            target: 'userinterface',
            provider: 'activeaar'
        });
	
	me.main.server.setSafeMode(true,'true');
	
    },
    
    completedtestlocationCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("{completedtestlocation}",rsp.objects[0].a);
        // Create object for the completed test location phone call
        me.main.data.create({
            callback: me.completedtestphoneCallback,
            component: rsp.cmp,
            scope: me,
            data: [{
                cid: 'phonecall',
                l: 'Completed Test Phone Call',
                pa: rsp.objects[0].a,
                t: 0
            }]
        });
    },

    completedtestphoneCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.main.data.setProvider("{completedtestphonecall}",rsp.objects[0].a);
    },
    
    /* -----------------------------------------------------------------------
    aarchaperone:
    Toggles disabled/enabled state of chaperone roles fieldset in aar
    __________________________________________________________________________ */
    
    aarchaperoneHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var roles = this.main.util.get('aarrolesfs');
        
        if (aar.AfterActionChapPresent=="Yes") {
            roles.enable(true);
        } else {
            roles.disable(true);
        }
        
        this.main.rules.broadcastTopic({topic:'requiredchange'});
    },
    
     /* -----------------------------------------------------------------------
    athleteuncooperative:
    Sets response field to required if athlete is uncooperative
    __________________________________________________________________________ */
       
    athleteuncooperativeHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var comments = this.main.util.get('AAProcessingComments');
        
        if (aar.AfterActionCooperative=="No") {
           comments.required=true;
        } else {
            comments.required=false;
        }

        this.main.rules.broadcastTopic({topic:'refresh',component:comments});
        this.main.rules.broadcastTopic({topic:'requiredchange'});
        
    },
    
    /* -----------------------------------------------------------------------
    submitaar:
    Submits an After Action Report (AAR): creates AAR document
    __________________________________________________________________________ */
    
    submitaarHandler: function (sp) {
        var aar = this.main.data.getProvider("activeaar");
        var ats = this.main.data.getProvider("activetestsession");
        // timestamp
        var ts = new Date();

        // Get the active arr and it's descendants
        var report = this.main.data.map[aar.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('AAROutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        // Create the After Action Report inside of the active test session
        this.main.data.create({
            callback: this.submitaarFeedback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'After Action Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'aar',
                ct: output,
                a: ats.a + '.aar'
            }]
        });
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            AfterAction_completed: 1
        });
    },
    
    submitaarFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        //Ext.defer(function () {
        //    this.main.feedback.say({
        //        title: 'Success',
        //        message: 'After Action Report saved.'
        //    })
        //},
        //1200,
        //this)
    },
    
    /* -----------------------------------------------------------------------
    submitsession:
    Saves the test session: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */
    
    submitsessionHandler: function (sp) {

	// Disable Submit button so user doesn't press it twice.
        sp.component.setDisabled(true);

        var ats = this.main.data.getProvider("activetestsession");
	var ath = this.main.data.getProvider("activeathlete");

        var ts = new Date();
	var ds = Ext.Date.format(ts,'m-d-Y h:i A');
	

	// Format the athletes name
	var apn = ''; 
	var fn = this.main.data.getDerivedValue(ath,'_FirstName') || ath.FirstName;
	var ln = this.main.data.getDerivedValue(ath,'_LastName') || ath.LastName;
        var apnfl = apn.concat(fn,', ',ln);
	var apnlf = apn.concat(ln,', ',fn);
	var apncmb = apn.concat(fn,' ',ln);

	// Get the athlete's email
	var pemail = this.main.data.getDerivedValue(ath,'_PrimaryEmail');

	/*-------------------------------------------------------------
	 * Process the samples connected to this test session
	 * __________________________________________________________*/

        var desc = this.main.data.getDescendants(ats.ca);
        
        // Get the lab screens for the test session
        var sarr = this.main.data.filter(desc, 'cid EQ labscreen');

	// TODO this is a good spot to bail if there aren't any screens on the test
	
        // If Urine flag is set then get the urine samples for the test session
        var uarr = [];
	if (ats.Urine) uarr = this.main.data.filter(desc, 'cid EQ urinesample');

        // If DBS flag is set then get the DBS samples for the test session
        var dbsarr = [];
	if (ats.DBS) dbsarr = this.main.data.filter(desc, 'cid EQ dbssample');

	// If Blood flag is set then get the blood samples for the test session
        var barr = [];
	if (ats.Blood) barr = this.main.data.filter(desc, 'cid EQ bloodsample');

	// Copy all of the lab screen values to the appropriate samples
	this.applylabscreens(uarr,barr,dbsarr,sarr);
	
	// Set additional properties on the urine samples
	if (uarr.length>0) this.applyadditionalsampleproperties(uarr,'UrineSealedDate','UrineSealedTime','UrineSampleCode',ats.IsExpeditedAnalysis);

	// Set additional properties on the blood samples
	if (barr.length>0) this.applyadditionalsampleproperties(barr,'BloodSealedDate','BloodSealedTime','BloodSampleCode',ats.IsExpeditedAnalysis);

	// Set additional properties on the DBS samples
	if (dbsarr.length>0) this.applyadditionalsampleproperties(dbsarr,'DBSProcessingDate','DBSProcessingTime','DBSSampleCode',ats.IsExpeditedAnalysis);
	
	// Add the athletes age to the blood and dbs samples
        var age = '';
        //if (!Ext.isEmpty(ath.DOB)){
        //    age = this.main.util.getAge(ath.DOB,ts);
        //}
	var bday = this.main.data.getDerivedValue(ath,'_DOB');
	if (!Ext.isEmpty(bday)){
		age = this.main.util.getAge(bday,ts);
	}

	var cmbarr = barr.concat(dbsarr);
	this.main.actions.perform({
            cmd: 'put',
            component: this,
            objects: cmbarr,
            Age: age
        });
                
	/*--------------------------------------------------------------
	 * Process the paperwork for the test session
	 * ___________________________________________________________*/

        // Get the active test session and it's descendants
        var report = this.main.data.map[ats.a];
        
        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('dcfReview').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
	output = output.concat(this.main.localization.translate('languagedisclaimer'), ds, "</div>");

	var combinedoutput = ''; 
	// If athlete is UFC or Professional Boxing create the dcor receipt
	if (ats.Sport=='155'||ats.Sport=='139') {
		var receipttpl = new Ext.XTemplate(this.main.data.getObjectByName('dcorReceipt').ct,this.main.util.getTemplateFunctions());
		var receipt = receipttpl.apply(report);
		combinedoutput = output.concat(receipt);
	}
        var dcorlbl = 'DCOR ';
       	dcorlbl = (ats.designation=='nonevent') ? dcorlbl.concat(ds) : dcorlbl.concat(apnfl, ' - ', ds);

	// If not a paper based test, did athlete elect to receive forms by email?
	var senddcor = (ats.papertest) ? false : ats.emailforms;

	// Set up the message for the email
	var emailmsg = this.main.localization.translate('athleteemailmessage');
	var surveylink = (ats.Sport=='155') ? '<a href="https://www.surveymonkey.com/r/2016UFCAthlete?DCOID='+ ats.DCOPNFinal + '"><b>ATHLETE SURVEY</b></a>'
 : '<a href="https://www.surveymonkey.com/r/Athlete2016?DCOID='+ ats.DCOPNFinal + '"><b>ATHLETE SURVEY</b></a>';

	// Need to create the DCOR and Receipt combo to email to UFC and Proboxing athletes
	if (combinedoutput!='') {
		var receiptlbl = apnlf.concat(' - ', ds); 

		// This will be emailed to athlete instead of dcor
		senddcor = false;

		// Create the combined DCOR/Receipt document to send to the athlete
		this.main.data.create({
			callback: Ext.emptyFn,
			component: this,
			scope: this,
			data: [{
				cid: 'document',
				pa: ats.a,
				l: receiptlbl.concat(' - DCOR Combined'),
				t: 0,
				notification: pemail,
				AthletePrintedName: apncmb,
				message: emailmsg.concat(surveylink),
				subject: this.main.localization.translate('athleteemailsubject'),
				emailforms: ats.emailforms,
				timestamp: ts,
				doctype: 'dcorcombined',
				ct: combinedoutput,
				a: ats.a + '.dcorcombined'
			}]
		});

		// Create the DCOR Receipt to send to USADA
		this.main.data.create({
			callback: Ext.emptyFn,
			component: this,
			scope: this,
			data: [{
				cid: 'document',
				pa: ats.a,
				l: receiptlbl.concat(' - DCOR Receipt'),
				t: 0,
				notification: 'DCORReceipts@usada.org',
				AthletePrintedName: apncmb,
				message: receiptlbl.concat(' - DCOR Receipt'),
				subject: receiptlbl.concat(' - DCOR Receipt'),
				emailforms: true,
				timestamp: ts,
				doctype: 'dcorreceipt',
				ct: receipt,
				a: ats.a + '.dcorreceipt'
			}]
		});
	}

	// Remove the links for the previous declarations
	this.processdoulinks(sp,ats,ath);
	

        var cb = (ats.designation=='event') ? Ext.bind(this.setrostercompleteCallback,this,[ats,ts],1) : Ext.bind(this.submitsessionCallback,this,[ats,ts],1);
       // Create the DCOR object. Only email this if the athlete has opted to receive it and they are not a UFC or Professional Boxing athlete 
        this.main.data.create({
            //callback: this.submitsessionCallback,
	    callback: cb,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: dcorlbl,
                t: 0,
                notification: pemail,
		AthletePrintedName: apncmb,
		message:emailmsg.concat(surveylink), 
		subject: this.main.localization.translate('athleteemailsubject'),
		emailforms: senddcor,
                timestamp: ts,
                doctype: 'dcor',
                ct: output,
                a: ats.a + '.dcor'
            }]
        });
        
	
	// Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            _Status: 'COMPLETED',
            DocumentGenerated: ts
        //    CheckedOut: 'Completed'
        });

        sp.component.setDisabled(false);
        
    },
    
    submitsessionCallback: function (rsp,ats,ts) {
        var me = (rsp.scope||this);
        
	//// Update test session status
        //me.main.actions.perform({
        //    cmd: "put",
        //    component: rsp.component,
        //    objects: [ats],
        //    _Status: 'COMPLETED',
        //    DocumentGenerated: ts
        //});


        me.gopreviousdashboardHandler(rsp);
        
        //Ext.defer(function () {
        //    me.main.feedback.say({
        //        title: 'Success',
        //        message: 'Test Session saved.'
        //    })
        //},
        //1200,
        //me)
    },
    
    setrostercompleteCallback: function (rsp,ats,ts) {
        var me = (rsp.scope||this);
        
	// Update test session status
        me.main.actions.perform({
            cmd: "put",
            component: rsp.component,
            objects: [ats],
            //_Status: 'COMPLETED',
            //DocumentGenerated: ts,
            CheckedOut: 'Completed'
        });


        // Get an array of all the samples attached to the manifest
	var desc = me.main.data.getChildren(me.main.data.getProvider("activeevent").ca);
        //var desc = rsp.leaf.par.children;
        var srarr = me.main.data.filter(desc, 'cid EQ siteroster');
        var ar = '';

	for (var i=0,l=srarr.length;i<l;i++) {
		if (srarr[i].testalias==ats.ca){
		       	ar=srarr[i];
			break;
		}
	}

	if (ar) {

            me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                RosterHistory: 'Completed',
                timestamp: ts
            });
               
           // Create a ledger entry to check athlete into site roster
            me.main.data.create({
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'ledgerentry',
                    l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ar.a,
                    t: 0,
                    Timestamp: ts,
                    RosterHistory: 'Completed'
                }]
            });
	}
	me.gopreviousdashboardHandler(rsp);
        
        //Ext.defer(function () {
        //    me.main.feedback.say({
        //        title: 'Success',
        //        message: 'Test Session saved.'
        //    })
        //},
        //1200,
        //me)

    },
    
    /*-----------------------------------------------------------------------
     * apply lab screens  
     * Applies the screens to the samples 
     * ____________________________________________________________________*/

    	
    applylabscreens: function (usamples,bsamples,dbssamples,screenarr) {

    	var up = {
            cmd: 'put',
            component: this,
            objects: usamples
        };
        
	var dbsp = {
            cmd: 'put',
            component: this,
            objects: dbssamples
        };
        
	var garr = [];
	var parr = [];

        for (var i=0,l=bsamples.length;i<l;i++) {
	    // Sort blood samples by kit color so we can apply screens
	    if (bsamples[i].SampleKitColor=="Purple") parr.push(bsamples[i]);
	    if (bsamples[i].SampleKitColor=="Gold") garr.push(bsamples[i]);

        }
	
	var gp = {
		cmd: 'put',
		component: this,
		objects: garr
	};

	var pp = {
		cmd: 'put',
		component: this,
		objects: parr
	};
        
        // Add screens to the samples
        for (var i=0,l=screenarr.length;i<l;i++) {
            var sname = this.main.localization.lookupRawValue("screen",screenarr[i].ScreenTypeId,"ShortName");
	    var tmethod = this.main.localization.lookupRawValue("screen",screenarr[i].ScreenTypeId,"DefaultTestMethod");
            if (/Urine/.test(String(tmethod))) {
                switch (String(screenarr[i].ScreenTypeId)) {
                    case '1':
                        up[sname] = 'full';
                        break;
                    case '2':
                        up[sname] = 'partial';
                        break;
                    default:
                        up[sname] = screenarr[i].LabId;
                        up[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
                }
            } else if (/DBS/.test(String(tmethod))) {
		    dbsp[sname] = screenarr[i].LabId;
		    dbsp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
            } else if (/Blood\b/.test(String(tmethod))) {
		switch (screenarr[i].ScreenTypeId){
		    case 4:
			    gp[sname] = screenarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    break;
		    case 7:
			    pp[sname] = screenarr[i].LabId;
			    pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    break;
		    case 8:
			    pp[sname] = screenarr[i].LabId;
			    pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    break;
		    case 11:
			    gp[sname] = screenarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = screenarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    }
			    break;
		    case 12:
			    gp[sname] = screenarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = screenarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    }
			    break;
		    case 15:
			    gp[sname] = screenarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    break;
		    case 17:
			    gp[sname] = screenarr[i].LabId;
			    gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    if (garr.length==0){
			    	pp[sname] = screenarr[i].LabId;
			    	pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			    }
			    break;
		    default:
            		if (/Blood-Serum/.test(String(tmethod))) {
				gp[sname] = screenarr[i].LabId;
				gp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"Shortname");
			} else if (/Blood-EDTA/.test(String(tmethod))) {
				pp[sname] = screenarr[i].LabId;
				pp[sname+'_caption'] = this.main.localization.lookupRawValue("testinglab",screenarr[i].LabId,"ShortName");
			}
			break;
		}
	    }
        }

        if (usamples.length>0) this.main.actions.perform(up);
	if (dbssamples.length>0) this.main.actions.perform(dbsp);
	if (garr.length>0) this.main.actions.perform(gp);
	if (parr.length>0) this.main.actions.perform(pp);
        
    },

	
	
    /*---------------------------------------------------------------------
     * Apply 
     * Calculate UTC time for sample sealed/processing date and time,
     * set the label to the sample code, and set the Sample Status
     * as AVAILABLE so that it can be added to a sample manifest
     * __________________________________________________________________*/

     applyadditionalsampleproperties: function (samplearr,dateprop,timeprop,codeprop,isexpedited){

	var expeditedVal = (isexpedited==1) ? true : false;
	// Calculate the UTC date/time and set object label to sample code
        for (var i=0,l=samplearr.length;i<l;i++) {
            var tempdate = samplearr[i][dateprop];
            var temptime = samplearr[i][timeprop];
            if (Ext.isDate(tempdate)&&Ext.isDate(temptime)) {
		// Write the Process date and time values to the sample
		var ptime= new Date(tempdate.getFullYear(), tempdate.getMonth(),tempdate.getDate(),temptime.getHours(),temptime.getMinutes(), temptime.getSeconds());
		var pdate= new Date(tempdate.getFullYear(), tempdate.getMonth(),tempdate.getDate(),temptime.getHours(),temptime.getMinutes(), temptime.getSeconds());
                // Calculate UTC date/time
                var utcdate = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());
                var utctime = new Date(tempdate.getUTCFullYear(), tempdate.getUTCMonth(), tempdate.getUTCDate(), temptime.getUTCHours(), temptime.getUTCMinutes(), temptime.getUTCSeconds());

            	this.main.actions.perform({
            	    cmd: "put",
            	    component: this,
            	    objects: [samplearr[i]],
	    	    l: samplearr[i][codeprop],
            	    SampleStatus: 'AVAILABLE',
	    	    IsExpeditedAnalysis: expeditedVal,
		    ProcessDate: this.main.util.dateToISO8601(pdate),
		    ProcessTime: this.main.util.dateToISO8601(ptime),
            	    ProcessDate_gmt: this.main.util.dateToISO8601(utcdate),
            	    ProcessTime_gmt: this.main.util.dateToISO8601(utctime)
            	});
           
            }
        }
     },

    /*-----------------------------------------------------------------------
     * testdoutoathletelink
     * Function to test linking dou items to athlete
     * ____________________________________________________________________*/
    testdoutoathletelinkHandler: function (sp) {

	//this.removedoulinks(sp);
	//return;

        var ats = this.main.data.getProvider("activetestsession");
	var desc = this.main.data.getDescendants(ats.ca);
        
	var now = new Date();
        var ath = this.main.data.getProvider("activeathlete");

        var douarr = this.main.data.filter(desc, 'cid EQ declaration');
	this.linkdoutoathlete(ats.designation,ath,douarr);
    },

    /*------------------------------------------------------------------------
     * Remove dou links
     * Removes all of the declarations linked to the athlete record
     * _____________________________________________________________________*/

    processdoulinks: function (sp,ats,ath) {
	//var ath = this.main.data.getProvider("activeathlete");
	var desc = this.main.data.getDescendants(ath.ca);
        var dou = this.main.data.filter(desc, 'cid EQ declaration');

	// We only want to remove the declarations that match the designation
	// of the active test session
	var designation = (ats.designation=='nonevent') ? 'OOC' : 'IC';
   
	for (var i=0,l=dou.length;i<l;i++) {
		if (designation == dou[i].Designation){		
			this.main.data.move({
        		    component: sp.component,
        		    scope: this,
        		    source: dou[i],
        		    destination: this.main.environment.get("RECYCLEBIN")
        		});
		}
	} 
	
	this.linkdoutoathlete(designation,ath,ats);

    },
 
    /*------------------------------------------------------------------------
     * link dou to athlete
     * Links the declarations in the completed test session to the ahtlete
     * so that the previous delcarations will appear the next time the 
     * ahtlete is tested in Paperless
     * ____________________________________________________________________*/
    linkdoutoathlete: function (designation,ath,ats) {

       	    var desc = this.main.data.getDescendants(ats.ca); 
	    var douarr = this.main.data.filter(desc, 'cid EQ declaration');

	    for (var i=0,l=douarr.length;i<l;i++) {
		this.main.data.createLink({
        	    parent: ath,
        	    child: douarr[i],
		    data: [
		    	{
				Designation: designation,
				DOUStatus:'AVAILABLE'
			}
		    ]
        	});
	    }

    },
    
    removedoulinksHandler: function (sp) {
	    this.processdoulinks(sp);
    },

  

    /* -----------------------------------------------------------------------
    submit completed tests :
    Check that all required docs are completed, create json packet, update 
    test session status
    __________________________________________________________________________ */
    
    submitcompletedtestsHandler: function (sp) {
    
        sp.component.setDisabled(true);
	    
	var gname = (sp.component.n=='submitOOCbutton') ? 'ooccompletedtestsgrid' : 'iccompletedtestsgrid';
        var tgrid = this.main.util.get(gname);
	if (Ext.isEmpty(tgrid)){
		console.log("Can't find completed test session grid");
		return;
	}

        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var earray = [];
        var tlist='';
        
        // Check that required docs are complete
        for (var i=0,l=data.length;i<l;i++) {
            var upass = false, spass = false, apass = false;
            if (data[i].selectedtestsession) {
                if (data[i].UAR_completed) {
                    upass = true;
                } else {
                    if (data[i].SampleShipment_completed) {
                        spass = true;
                    } else {
                        earray.push("All samples for " + data[i].FirstName + ' ' + data[i].LastName + " test session have not been shipped.");
                    }
                    if (data[i].designation=='event' || data[i].AfterAction_completed) {
                        apass = true;
                    } else {
                        earray.push("After action report for " + data[i].FirstName + ' ' + data[i].LastName + " test session has not been completed.");
                    }
                }
           
                if (upass || (spass && apass)) tarray.push(data[i]);
            }
        }
        
        if (earray.length > 0) {
            var elist = earray.join('<br>');
            this.main.feedback.say({
                title: 'Submission Cancelled',
                message: 'Some of the selected tests could not be submitted:<br>' + elist
            });
	    sp.component.setDisabled(false);
            return;
        }
        
	if (tarray.length==0) {
		this.main.feedback.say({
			title: 'Error',
			message: 'You must select at least one test session to submit.'
		});
		sp.component.setDisabled(false);
		return;
	}

	var farray = [];
        for (var i=0,l=tarray.length;i<l;i++) {
            // Get the raw objects for test session and docs
            // otherwise json encode will fail
            var ts = [];
            var desc = this.main.data.getDescendants(tarray[i].a);
	    var tfail = false;
            for (index in desc) {
                var o = this.main.data.getObject(desc[index].a);
		// make sure the object isn't empty
		if (Ext.isEmpty(o)){
			tfail = true;
			break;
		} else {
			ts.push(o);
		}
            }
	    // found an empty object - bail on submitting this test and move to next one
	    if (tfail){ 
		    farray.push('Failed to get associated data for ' + tarray[i].FirstName + ' ' + tarray[i].LastName + ' test session. Please check Internet connection and try again.');
		    continue;
	    }
		
	    try {
            	var s = Ext.JSON.encode(ts);
	    }
	    catch(err) {
		   farray.push('Failed to create test packet for ' + tarray[i].FirstName + ' ' + tarray[i].LastName + ': ' + err); 
		    continue;
	    }
           
            // write json data to completed tests folder
            this.main.data.create({
                component: sp.component,
                scope: this,
                data: [{
                    cid: 'testpacket',
                    l:  tarray[i].TSID,
                    pa: this.main.environment.get("COMPLETEDTESTS_FOLDER"),
		    n: 'testpacket.' + tarray[i].TSID,
                    t: 0,
                    ct: s,
		    a: tarray[i].a + '.testpacket',
		    ix: i
                }]
            });
           
           
            // Update the status of the test session
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [tarray[i]],
                _Status: 'CLOSED'
            });
           
        }
        
	sp.component.setDisabled(false);
	var donetitle = 'Success';
	var donemsg = 'Selected tests have been submitted.';
	if (farray.length > 0) {
                var flist = farray.join('<br>');
		donetitle = 'Error';
		donemsg = 'Some of the selected tests could not be submitted:<br>' + flist;

	} 
        
        var cb = Ext.bind(this.submitcompletedtestCallback,this,[tgrid],1);
	Ext.Msg.show({
		title: donetitle,
		message: donemsg,
		buttons: Ext.Msg.OK,
		fn: cb
	});

    },

    submitcompletedtestCallback: function (rsp,tgrid) {
	var me = (rsp.scope||this);
	me.main.cache.invalidate(tgrid.sourcealias);
	tgrid.loadData();
    },
     
    /* -----------------------------------------------------------------------
    submit completed IC tests :
    Check that all required docs are completed, create json packet, update 
    test session status
    __________________________________________________________________________ */
    
   submitcompletedictestsHandler: function (sp) {
    
        sp.component.setDisabled(true);
	
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')) {
		// do nothing
	} else {
		this.main.feedback.say({
			title: 'Error Submitting Tests',
			message: 'You must be online to submit completed in-competition tests.'
		});
		sp.component.setDisabled(false);
		return;
	}
	    
        var tgrid = this.main.util.get('iccompletedtestsgrid');
	if (Ext.isEmpty(tgrid)){
		console.log("Can't find completed test session grid");
		sp.component.setDisabled(false);
		return;
	}

        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var earray = [];
        var tlist='';
        
        // Check that required docs are complete for the selected tests
        for (var i=0,l=data.length;i<l;i++) {
            var spass = false;
            if (data[i].selectedtestsession) {
                
                if (data[i].SampleShipment_completed) {
                    spass = true;
                } else {
                    earray.push("All samples for " + data[i].FirstName + ' ' + data[i].LastName + " test session have not been shipped.");
                }
           
                if (spass) tarray.push(data[i]);
            }
        }
        
	// Not all tests were complete - bail
        if (earray.length > 0) {
            var elist = earray.join('<br>');
            this.main.feedback.say({
                title: 'Submission Cancelled',
                message: 'Some of the selected tests could not be submitted:<br>' + elist
            });
	    sp.component.setDisabled(false);
            return;
        }
        
	// User didn't select anything to submit
	if (tarray.length==0) {
		this.main.feedback.say({
			title: 'Error',
			message: 'You must select at least one test session to submit.'
		});
		sp.component.setDisabled(false);
		return;
	}

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='iccompletedtestsgrid']"),
		message: 'Processing test sessions...'
	});

	var cb = Ext.bind(this.processtestdataCallback,this,[tarray,sp.component],1);
	var fcb = Ext.bind(this.processtestdatafailureCallback,this,[sp.component],1);
	
	var ae = this.main.data.getProvider('activeevent');

	this.main.actions.perform({
       	    cmd: 'request',
       	    a: ae.ca,
       	    relationship: 'descendants',
       	    callback: cb,
	    failure: fcb,
       	    scope: this,
       	    component: sp.component
       	});

    },   

    processtestdatafailureCallback: function (rsp,cmp) {
	    var me = (rsp.scope||this);

	    cmp.setDisabled(false);

	    me.main.actions.perform({
		    cmd: 'removebusy',
		    component: me.main.view.down("*[name='iccompletedtestsgrid']")
	    });

	    me.main.feedback.say({
		    title: 'Error',
		    message: 'Could not retrieve event information.  Please try again.'
	    });
    },

    processtestdataCallback: function (rsp,tarray,cmp) {
    
        var me = (rsp.scope||this);
	var ts = rsp.leaf;
	var desc = rsp.desc;

	var farray=[];
	var testsprocessed = 0;
	var cb = Ext.bind(me.completeictestdataCallback,rsp.scope,[rsp,cmp,farray],1);

	tarray.forEach(function(item, index, array) {
            var tsarr = [me.main.data.getObject(item.ca)];
	    var tsdata = me.main.data.filter(desc, 'ca EQ ' + item.ca);
	    if (tsdata.length>0) {
	    	var tsitems = me.main.data.getChildren(tsdata[0].a);

            	// Get the raw objects for test session and docs
            	// otherwise json encode will fail
		var tfail=false;
            	for (i in tsitems) {
		    if (tsitems[i].cid=='declaration'&&tsitems[i].t==1){
			    //skip the linked declarations
			    break;
		    } 
            	    var o = me.main.data.getObject(tsitems[i].a);
            	    // make sure the object isn't empty
		    if (Ext.isEmpty(o)){
		    	tfail = true;
		    	break;
		    } else {
		    	tsarr.push(o);
		    }
                }
	        // found an empty object - bail on submitting this test and move to next one
	        if (tfail){ 
	                farray.push('Failed to get associated data for ' + tsitems[i].FirstName + ' ' + tsitems[i].LastName + ' test session. Please check Internet connection and try again.');
	        } else {
	            
	        	try {
                		var s = Ext.JSON.encode(tsarr);
	        	}
	        	catch(err) {
	        	       farray.push('Failed to create test packet for ' + tarray[i].FirstName + ' ' + tarray[i].LastName + ': ' + err); 
	        	        testsprocessed++;
	        	        return;
	        	}

            		// write json data to completed tests folder
            		me.main.data.create({
            		    component: rsp.component,
            		    scope: me,
            		    data: [{
            		        cid: 'testpacket',
	    		        l: 'ICtestpacket.' + item.a, 
            		        pa: me.main.environment.get("COMPLETEDTESTS_FOLDER"),
	    		        n: 'testpacket.' + item.a,
            		        t: 0,
            		        ct: s,
	    		        a: item.a + '.testpacket',
				ix: index
            		    }]
            		});
           
           
            		// Update the status of the test session
            		me.main.actions.perform({
            		    cmd: "put",
            		    component: me,
            		    objects: [item],
            		    _Status: 'CLOSED'
            		});
		}
	    }
	    
	    // when all tests are processed call the complete callback
	    testsprocessed++;
	    if (testsprocessed===array.length){
		    cb();
	    }
           
        });
    },

    completeictestdataCallback: function (rsp,cmp,farray) {
        var me = (rsp.scope||this);

	// Enable the submit button
	cmp.setDisabled(false);

	// Remove busy indicator off of grid
	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='iccompletedtestsgrid']")
	});

	var donetitle = 'Success';
	var donemsg = 'Selected tests have been submitted.';
	if (farray.length > 0) {
                var flist = farray.join('<br>');
		donetitle = 'Error';
		donemsg = 'Some of the selected tests could not be submitted:<br>' + flist;

	} 
        
        var cb = Ext.bind(me.submitcompletedictestCallback,me);
	Ext.Msg.show({
		title: donetitle,
		message: donemsg,
		buttons: Ext.Msg.OK,
		fn: cb
	});
    },

    submitcompletedictestCallback: function (rsp) {

        var me = (rsp.scope||this);
	// Refresh the grid
        var tgrid = me.main.util.get('iccompletedtestsgrid');
	me.main.cache.invalidate(tgrid.sourcealias);
	tgrid.loadData();

    },
    
    
    
    /* -----------------------------------------------------------------------
    submitpostsupplementary:
    Saves the post supplementary report: Updates the test session status to COMPLETED; creates
    dcor document; Sends email to athlete; builds samples
    __________________________________________________________________________ */

    submitpostsupplementaryHandler: function (sp) {
        
        var aps = this.main.data.getProvider("activepostsupplementary");
        var ats = this.main.data.getProvider("activetestsession");
        // timestamp
        var ts = new Date();

        // Get the active arr and it's descendants
        var report = this.main.data.map[aps.a];

        // Get the template from the dcf Review form and apply test session data to it
        var tpl = new Ext.XTemplate(this.main.data.getObjectByName('PostSuppOutput').ct,this.main.util.getTemplateFunctions());
        var output = tpl.apply(report);
        
        // Create the After Action Report inside of the active test session
        this.main.data.create({
            callback: this.submitpostsupplementaryFeedback,
            component: this,
            scope: this,
            data: [{
                cid: 'document',
                pa: ats.a,
                l: 'Post Supplementary Report ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'postsupp',
                ct: output
            }]
        });
        
        // Update test session status
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            PostSupplementary_completed: 1
        });
    },
    
    submitpostsupplementaryFeedback: function (rsp) {
        var me = (rsp.scope||this);
        
        me.gopreviousdashboardHandler(rsp);
        
        //Ext.defer(function () {
        //    this.main.feedback.say({
        //        title: 'Success',
        //        message: 'Post Supplementary Report saved.'
        //    })
        //},
        //1200,
        //this)
    },

    /* -----------------------------------------------------------------------
    setenglish:
    Changes the internal language to english
    __________________________________________________________________________ */

    setenglishHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the language to English'
//        });
        this.main.environment.set("language","en-us","local");
        this.main.data.reset();
        this.main.render.reload();
    },
    
    /* -----------------------------------------------------------------------
    setspanish:
    changes the internal language to spanish
    __________________________________________________________________________ */

    setspanishHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the languge to Spanish'
//        });
        this.main.environment.set("language","es-mx","local");
        this.main.data.reset();
        this.main.render.reload();
    },

    /* -----------------------------------------------------------------------
    setportuguese:
    Changes the internal language to portuguese
    __________________________________________________________________________ */

    setportugueseHandler: function (sp) {
//        this.main.feedback.say({
//            title: 'Change Language',
//            message: 'This will set the langugae to Portuguese'
//        });
        this.main.environment.set("language","pt-br","local");
        this.main.data.reset();
        this.main.render.reload();
    },
    
    /* -----------------------------------------------------------------------
    toggleonline:
    Toggles the app between offline and online mode
    __________________________________________________________________________ */
    
    //toggleonlineHandler: function (sp) {
    //    this.main.feedback.say({
    //        title: 'Online Indicator',
    //        message: 'This will toggle the app between online and offline mode'
    //    });
    //},
    
/* -----------------------------------------------------------------------
    showalerts:
    Display Paperless alerts
    __________________________________________________________________________ */
    
    showalertsHandler: function (sp) {
        /*this.main.feedback.say({
            title: 'Alerts',
            message: 'This will display any unviewed alerts'
        });*/
	if (this.main.environment.isNative()) {
            var v = AppVersion.version;
            var b = AppVersion.build;
	} else {
		v = 'only available in native build.';
		b = 'only available in native build.';
	}
        this.main.feedback.say({
            title: 'Paperless',
            message: 'Version ' + v + '<br>Build ' + b
        });

        //this.main.feedback.ask({
        //    title: 'Name of item you want to debug',
        //    mesaage: 'what item do you want?',
        //    callback: this.debugCallback,
        //    scope: this
        //});
        
    },

    debugHandler: function (sp) {
        
        this.main.feedback.ask({
            title: 'Name of item you want to debug',
            mesaage: 'What item do you want?',
            callback: this.debugCallback,
            scope: this
        });
    },
    
    debugCallback: function (sp,s) {
        debugger;
        var mainobject = this.main.data.getObject(s)||this.main.data.getObjectByName(s);
        var mappedobject = this.main.data.map[mainobject.a];
        var cachedobject;
        var me = this.main.cache;
        
        me.db.transaction(function (tx) {
            tx.executeSql("SELECT value FROM data WHERE a=?", [mainobject.a], function(tx, results) {
                for (var i=0,l=results.rows.length; i<l; i++) {
                    var row = results.rows.item(i);
                    cachedobject = me.decrypt(Ext.decode(row['value']));
                }
                debugger;
            });
        });
    },

    
    /* -----------------------------------------------------------------------
    logout:
    Logs user out of Paperless
    __________________________________________________________________________ */
    
    logoutuserHandler: function (sp) {
	    var v = AppVersion.version;
	    var b = AppVersion.build;
        this.main.feedback.say({
            title: 'Paperless',
            message: 'Version ' + v
        });
	cmd: logout
    },
    
    /* -----------------------------------------------------------------------
    startmanifest:
    Opens the mainfest form and displays waybill photo
    __________________________________________________________________________ */
    startmanifestHandler: function (sp) {
        var am = this.main.data.getProvider("activemanifest");
        
        //debugger;
        (sp.designation=='nonevent') ? this.main.environment.set("smsource",this.main.entity.getUser().a) : this.main.environment.set("smsource",this.main.data.getProvider("activeevent").a);
        
        var desc = this.main.data.getDescendants(am.ca);
        var parr = this.main.data.filter(desc, 'cid EQ document');
        
        var photo = this.main.data.getObjectByName('waybillphoto');
        var cbtn = this.main.data.getObjectByName('clearphotobtn');
           
        if (parr.length>0){
            photo.ct = parr[0].ct;
            cbtn.disabled = false;
           
           this.main.actions.perform({
                cmd: 'setbusy',
                component: this.main.view
            });
        } else {
            photo.ct = "";
            cbtn.disabled = false;
        }
        
        this.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'samplemanifestpanel',
            target: 'userinterface'
        });
        

    },
    
    /*------------------------------------------------------------------------
     * removemanifestcheck
     * Checks to see if any samples are attached to the manifest.  Won't allow
     * user to remove manifest until samples have been removed.
     * _____________________________________________________________________*/

    removemanifestcheckHandler: function (sp) {
	var am = sp.value.data;

	var cb = Ext.bind(this.removemanifestcheckCallback,this,[sp.callback,sp.value,sp.idx],1);
	var fcb = Ext.bind(this.removemanifestfailureCallback,this,[sp.callback,sp.value,sp.idx],1);

        // find the samples that were linked to the manifest
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')) {
        	this.main.actions.perform({
        	    cmd: 'request',
        	    a: am.ca,
        	    relationship: 'stepparents',
        	    callback: cb,
		    failure: fcb,
        	    scope: this,
        	    component: sp.component
        	});
	} else {
		Ext.callback(cb,this,[{
			leaf: {stepparents: this.main.data.getStepParents(am)},
			scope: this,
			component: sp.component
		}]);
	}

    },

    removemanifestfailureCallback: function (rsp,cb,value,idx) {
	    var me = (rsp.scope||this);
	    var am = rsp.a;

	    var cb2 = Ext.bind(me.removemanifestcheckCallback,me,[cb,value,idx],1);
	    Ext.callback(cb2,me,[{
		    leaf: {stepparents: me.main.data.getStepParents(am)},
		    scope: me,
		    component: rsp.component
	    }]);
    },

    removemanifestcheckCallback: function (rsp,cb,value,idx) {
	var me = (rsp.scope||this);
        var sparents = rsp.leaf.stepparents;
	var samplefound = false;

	sparents.forEach(function (item, index, array) {
		if (item.cid=='urinesample' || item.cid=='bloodsample' || item.cid=='dbssample'){
			samplefound = true;
		}
	});

	if (samplefound) {
		me.main.feedback.say({
			title: "Error Deleting Manifest",
			message: "All samples must be removed from this manifest before it can be deleted."
		});
	} else {
		Ext.callback(cb,me,[value,idx],1);
	}
    },
      
     /* -----------------------------------------------------------------------
    submitmanifest:
    Saves the Sample Manifest: creates manifest document; marks samples as 
    shipped;
    __________________________________________________________________________ */
    
    submitmanifestHandler: function (sp) {
	sp.component.setDisabled(true);
        var am = this.main.data.getProvider("activemanifest");
	var ae = this.main.data.getProvider("activeevent");

	var iccb = Ext.bind(this.manifesteventCallback,this,[sp.component],1);
	var icfcb = Ext.bind(this.manifesteventfailureCallback,this,[sp.component],1);
	var cb = Ext.bind(this.pullsamplesCallback,this);
	var fcb = Ext.bind(this.pullsamplesfailureCallback,this);

	if (am.designation=='event') {
		if (!this.main.environment.getOnline()||this.main.environment.get('safemode')) {
			this.main.feedback.say({
				title:'Error',
				message: 'Paperless must be in online mode to save the manifest for In-Competition events.'
			});
			sp.component.setDisabled(false);
			return;
		} else {
			this.main.actions.perform({
				cmd: 'request',
				a: ae.ca,
				relationship: 'descendants',
				callback: iccb,
				failure: icfcb,
				scope: this,
				component: sp.component
			});
		}
	} else {

	        // find the samples that were linked to the manifest
		if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')) {
        		this.main.actions.perform({
        		    cmd: 'request',
        		    a: am.ca,
        		    relationship: 'stepparents',
        		    callback: cb,
			    failure: fcb,
        		    scope: this,
        		    component: sp.component
        		});
		} else {
			Ext.callback(cb,this,[{
				leaf: {stepparents: this.main.data.getStepParents(am)},
				scope: this,
				component: sp.component
			}]);
		}
	}
    },
    
   manifesteventfailureCallback: function (rsp,cmp) {
	var me = (rsp.scope||this);

	cmp.setDisabled(false);

	me.main.feedback.say({
		title: 'Error',
		message: 'Cannot retrieve event data.  Please try again.'
	});
    },

    manifesteventCallback: function (rsp,cmp) {

	var me = (rsp.scope||this);
	var am = me.main.data.getProvider("activemanifest");
	var desc = rsp.desc; // active event
	var ae = rsp.leaf;

	Ext.callback(me.pullsamplesCallback,me,[{
		leaf: {stepparents: me.main.data.getStepParents(am)},
		scope: me,
		component: cmp
	}]);
    },

    pullsamplesfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);
	    var am = rsp.a;
	    var cb = Ext.bind(me.pullsamplesCallback,me);
   	    // server request failed - try to get the samples from memory 
	    Ext.callback(cb,me,[{
			leaf: {stepparents: me.main.data.getStepParents(am)},
			scope: me,
			component: rsp.component
		}]);

    },
    

    pullsamplesCallback: function (rsp) {
        var me = (rsp.scope||this);
        var am = me.main.data.getProvider("activemanifest");
        var ts = new Date();

        // Get an array of all the samples attached to the manifest
        var desc = rsp.leaf.stepparents;
	if (!Ext.isDefined(desc)) {
		me.main.feedback.say({
			title: 'Error',
			message: 'Cannot find any samples attached to the manifest.'
		});
		rsp.component.setDisabled(false);
		return;
	}
   
	var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        var barr = me.main.data.filter(desc, 'cid EQ bloodsample');
	var darr = me.main.data.filter(desc, 'cid EQ dbssample');
        var sarr = uarr.concat(barr,darr);
        
	// Check that user selected at least one sample
	if (sarr.length<1) {
		me.main.feedback.say({
                	title: 'Error',
                	message: 'Cannot find any samples attached to the manifest.'
            	});
		rsp.component.setDisabled(false);
		return;
	}

	// Get the active manifest and it's descendants
        var report = me.main.data.map[am.a];
        
        // Get the template and apply the manifest form data to it
        var tpl = new Ext.XTemplate(me.main.data.getObjectByName('SampleManifestOutput').ct,me.main.util.getTemplateFunctions());
        var output = tpl.apply(report);

        var cb = Ext.bind(me.createsmdocCallback,me,[sarr],1);

        // Create the sample manifest document
        me.main.data.create({
            callback: cb,
            component: rsp.component,
            scope: me,
            data: [{
                cid: 'document',
                pa: am.a,
                l: 'Sample Manifest ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                t: 0,
                timestamp: ts,
                doctype: 'samplemanifest',
                ct: output,
                a: am.a + '.samplemanifest'
            }]
        });
        
	rsp.component.setDisabled(false);
    },

    createsmdocCallback: function (rsp,sarr) {
        var me = (rsp.scope||this);
        var doc = rsp.objects[0];
        var am = me.main.data.getProvider("activemanifest");
        var cb = Ext.bind(me.processsampleCallback,me,[doc],1);
        
	var itemsProcessed = 0;
	sarr.forEach(function (item, index, array) {
		// Get the test session for the sample
		var ts = me.main.data.getParent(item);
 
        	//Build a lab xml template.
        	var obj = me.main.data.getObjectByName('labxmltemplate');
        	var s = (obj) ? obj.ct : '';
        	var tpl = new Ext.XTemplate(s,me.main.util.getTemplateFunctions());
 
		// write labxml and mark sample as shipped
        	me.main.actions.perform({
        	    cmd: 'put',
        	    component: me,
        	    objects: [item],
        	    SampleStatus: 'SHIPPED',
        	    DateShipped: am.ShipmentDate,
        	    ShippingLab: am.ShippingLab, 
		    ShippingLab_caption: me.main.localization.lookupValue('testinglab',am.ShippingLab,'ShortName'),
		    MonitorNumber: am.MonitorNumber,
        	    ct: tpl.apply(item)
        	});
        		
        	// link the sample manifest doc to the test session
        	// that the sample is parented by
        	me.main.data.createLink({
        	    parent: ts,
        	    child: doc
        	});
               
        	// get all of the samples for the test session
        	var desc = me.main.data.getDescendants(ts.ca);
        	var uarr = me.main.data.filter(desc, 'cid EQ urinesample');
        	var barr = me.main.data.filter(desc, 'cid EQ bloodsample');
		var darr = me.main.data.filter(desc, 'cid EQ dbssample');
        	var samplearr = uarr.concat(barr,darr);
        	var done = true;
        		
		// check to see if each sample in test session has been shipped
        	for (var i=0,l=samplearr.length;i<l;i++) {
        	   if (samplearr[i].SampleStatus=='SHIPPED'||samplearr[i].SampleStatus=='SENT'){
			  // do nothing
		   } else {
			   done = false;
		   }
        	}
        		
		// mark samples completion on test
        	if (done) {
        	    me.main.actions.perform({
        	        cmd: 'put',
        	        component: me,
        	        objects: [ts],
        	        SampleShipment_completed: true
        	    });
        	}

		// when all samples associated with manfiest have been processed
		// then mark manifest as completed and go back to dashboard
		itemsProcessed++;
		if (itemsProcessed === array.length) {
			me.marksmcompletedCallback(me);
		}
	});
    },

    marksmcompletedCallback: function (rsp) {


	var am = this.main.data.getProvider("activemanifest");
          
        // Update the status of the manifest and move to the allmanifests folder
        this.main.actions.perform({
            cmd: "put",
            component: rsp,
            objects: [am],
            ManifestStatus: 'COMPLETED',
            pa: this.main.environment.get('ALLSAMPLEMANIFESTS_FOLDER')
        });
        
        this.gopreviousdashboardHandler(rsp);
    },
    
   
    /* -----------------------------------------------------------------------
    selectallitems:
    Selects all items in the grid. Expects following properties to be set 
    in the Select All button:
    + srcgrid: name of grid control
    + srccol: dataIndex of the xgridcheckcolumn to mark as selected
    __________________________________________________________________________ */
   
    selectallitemsHandler: function (sp) {

	// Check that the properties are set correctly on the Select All button
	if (Ext.isEmpty(sp.srcgrid) || Ext.isEmpty(sp.srccol)) {
		console.log(sp.component.n + ' not configured correctly: expecting srcgrid and srccol properties');
		return;
	}

	// Check that the source grid exists
   	var tgrid = this.main.util.get(sp.srcgrid);
	if (Ext.isEmpty(tgrid)){
	        console.log('cannot find '+sp.srcgrid);
       		return;
	}       

        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
	
	for (var i=0,l=data.length;i<l;i++) {
		data[i][sp.srccol]=true;
		store.getAt(i).set(sp.srccol,true);
	}

	grid.reconfigure(store);
    },

    /* ----------------------------------------------------------------------
     * save dou selection
     * saves the selected previous declarations to the test session.  Expects
     * following properties to be set in the Add button:
     * + srcgrid: name of grid control with the selection
     * + srccol: dataIndex of the xgridcheckcolumn that contains the selection
     * + destgrid: the grid to copy the declarations to
     * ____________________________________________________________________*/

    savedouselectionHandler: function (sp) {
  	if (Ext.isEmpty(sp.srcgrid) || Ext.isEmpty(sp.srccol) || Ext.isEmpty(sp.destgrid)) {
		console.log(sp.component.n + ' not configured correctly: expecting srcgrid, srccol, destgrid properties');
		return;
	}

        var ats = this.main.data.getProvider("activetestsession");

        var tgrid = this.main.util.get(sp.srcgrid);
	if (Ext.isEmpty(tgrid)){
		console.log('cannot find ' + sp.srcgrid);
		return;
	}

        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
       // Get the selected items
        for (var i=0,l=data.length;i<l;i++) {
            if (data[i][sp.srccol]) {
        
		this.main.data.create({
                	callback: Ext.emptyFn, 
                	component: sp.component,
                	scope: this,
                	data: [{
                	    cid: 'declaration',
                	    l:  'declaration',
                	    pa: ats.ca,
                	    t: 0,
                	    SubstanceName: data[i].SubstanceName,
			    ix: i
                	}]
            	});

		this.main.actions.perform({
		    cmd: "put",
                    component: this,
                    objects: [data[i]],
                    DOUStatus: 'SELECTED'
                });

	    }
	}

	// Refresh the grids
	this.main.cache.invalidate(tgrid.sourcealias);
        tgrid.loadData();
        this.main.util.get(sp.destgrid).loadData();
        

    },

    
    /* -----------------------------------------------------------------------
    savesampleselection:
    Saves the selected Samples to the sample manifest. Expects following 
    properties to be set in the Add Selected Samples button
    + srcgrid: name of the grid control with the sample selection
    + srccol: name of the xgridcheckcolumn that contains the selected items
    + destgrid: name of the grid control that shows samples included on manifest
    + analysisgrid: name of the analysis grid for the sample type
    __________________________________________________________________________ */
    
    savesampleselectionHandler: function (sp) {

	if (Ext.isEmpty(sp.srcgrid) || Ext.isEmpty(sp.srccol) || Ext.isEmpty(sp.destgrid) || Ext.isEmpty(sp.analysisgrid)) {
		console.log(sp.component.n + ' not configured correctly: expecting srcgrid, srccol, destgrid, and analysisgrid properties');
		return;
	}

        var am = this.main.data.getProvider("activemanifest");
        //var auc = this.main.data.getProvider("activeurinecompleted");

        var tgrid = this.main.util.get(sp.srcgrid);
	if (Ext.isEmpty(tgrid)){
		console.log('cannot find ' + sp.srcgrid);
		return;
	}

        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        
        // Get the selected urine samples
        for (var i=0,l=data.length;i<l;i++) {
            if (data[i][sp.srccol]) {
        
                // Link the selected sample to the sample manifest
                this.main.data.createLink({
                    parent: data[i],
                    child: am
                });
                
                // Update the status of the sample
                this.main.actions.perform({
                    cmd: "put",
                    component: this,
                    objects: [data[i]],
                    SampleStatus: 'SELECTED'
                });
	    }
	}
        
        // Refresh the grids
	this.main.cache.invalidate(tgrid.sourcealias);
        tgrid.loadData();
        this.main.util.get(sp.destgrid).loadData();
        this.main.util.get(sp.analysisgrid).loadData();
        
    },
 
    /* -----------------------------------------------------------------------
    removeselectedsample:
    Removes the selected sample from the sample manifest.  Expects following
    properties to be set on the Remove button within the grid:
    + srcgrid: name of the grid control that shows samples included on manifest
    + destgrid: name of grid control with the sample selection
    + analysisgrid: name of the analysis grid for the sample type
    __________________________________________________________________________ */
    
    removeselectedsampleHandler: function (sp) {
    
        var as = this.main.data.getProvider(sp.par.par.provider);
        
	var desc = this.main.data.getDescendants(as.ca);
        var asm = this.main.data.filter(desc, 'cid EQ samplemanifest');
   
        this.main.data.move({
            component: sp.component,
            scope: this,
            source: asm[0],
            destination: this.main.environment.get("RECYCLEBIN")
        });
        
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')) {
        	this.main.actions.perform({
        	    cmd: 'request',
        	    a: as.ca,
        	    relationship: 'item',
        	    callback: this.removeselectedsampleCallback,
		    failure: this.removeselectedsamplefailureCallback,
        	    scope: this,
        	    component: sp.component
        	});
	} else {
		Ext.callback(this.removeselectedsampleCallback,this,[{
			leaf: this.main.data.getSource(as.ca),
			scope: this,
			component: sp.component
		}]);
	}

    },

    removeselectedsamplefailureCallback: function (rsp) {
	    var me = (rsp.scope||this);
	    var as = rsp.a;

	    Ext.callback(me.removeselectedsampleCallback,me,[{
		    leaf: this.main.data.getSource(as.ca),
		    scope: me,
		    component: rsp.component
	    }]);
           
    },

    removeselectedsampleCallback: function (rsp) {
        var me = (rsp.scope||this);
           
        me.main.actions.perform({
            cmd: 'put',
            component: rsp,
            objects: [rsp.leaf],
            SampleStatus: 'AVAILABLE'
        });
        
      // Refresh the grids
        if (!Ext.isEmpty(rsp.component.srcgrid)) me.main.util.get(rsp.component.srcgrid).loadData();
        if (!Ext.isEmpty(rsp.component.destgrid)) me.main.util.get(rsp.component.destgrid).loadData();
        if (!Ext.isEmpty(rsp.component.analysisgrid)) me.main.util.get(rsp.component.analysisgrid).loadData();
    },
    
    /* -----------------------------------------------------------------------
    Waybill:
    Allows user to upload a photo of the waybill document for the sample manifest.
    User can take a photo of the waybill using the device camera, or select a
    photo from the photo library on the device
    __________________________________________________________________________ */
    
    selectwaybillphotoHandler: function (sp) {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
//        debugger;
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 0
         });
    },
    
    takewaybillphotoHandler: function () {
        var cb = Ext.bind(this.onPhotoURISuccess,this);
        navigator.camera.getPicture(cb, this.onPhotoFail, {
            quality: 50,
            destinationType: 0,
            encodingType: 0,
            targetWidth: 850,
            targetHeight: 1100,
            correctOrientation: false,
            sourceType: 1
        });
    },
    
    onPhotoURISuccess: function (dataURL) {

        this.photofile = dataURL;
        var htmldata = '<img src="data:image/jpeg;base64,' + dataURL + '" style="max-width:100%" />';
        var waybillphoto = this.main.util.get('waybillphoto');
        waybillphoto.ct = htmldata;
        
        waybillphoto.loadData();
        
        this.main.util.get('clearphotobtn').enable(true);
        this.main.util.get('uploadwaybillbtn').enable(true);
    },
    
    onPhotoFail: function (message) {
//        alert(message);
        this.fireEvent('cmd',{
            cmd: 'say',
            component: this,
            title: 'Photo',
            message: message
        });
    },
    
    deletephotoHandler: function (sp) {
        var waybill = this.main.util.get('waybillphoto');
        waybill.ct = '';
        waybill.loadData();
        
        this.main.util.get('clearphotobtn').disable(true);
        this.main.util.get('uploadwaybillbtn').disable(true);
    },
    
    uploadphotoHandler: function (sp) {
        var ts = new Date();
        var am = this.main.data.getProvider('activemanifest');
        var waybill = this.main.util.get('waybillphoto');
        sp.component.disable(true);
        
        var desc = this.main.data.getDescendants(am.ca);
        var parr = this.main.data.filter(desc, 'cid EQ document');
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: this.main.view
        });

        if (parr.length>0) {
            var w = parr[0];
            parr[0].ct = waybill.ct;
            // Update the image in the waybill document
            this.main.actions.perform({
                cmd: "put",
                component: this,
                objects: [w],
                ct: waybill.ct
            });
           this.uploadphotoCallback(sp);
        } else {
            // Create waybill document
            this.main.data.create({
                callback: this.uploadphotoCallback,
                component: this,
                scope: this,
                data: [{
                    cid: 'document',
                    pa: am.a,
                    l: 'Waybill ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    t: 0,
                    timestamp: ts,
                    doctype: 'waybill',
                    ct: waybill.ct,
                    a: am.a + '.waybill'
                }]
            });
        }
    },
    
    uploadphotoCallback: function (rsp) {
        // Create links to manifest
        var me = (rsp.scope||this);
        
        me.main.actions.perform({
            cmd: 'removebusy',
            component: me.main.view
        });
        
        Ext.defer(function () {
            me.main.feedback.say({
                title: 'Success',
                message: 'Waybill uploaded to server.'
            })
        },
        800,
        me)
        
    },
    
    /* -----------------------------------------------------------------------
     * Start Event
     * Loads the selected event into memory and opens the IC dashboard
     * _____________________________________________________________________*/

    starteventHandler: function (sp) {
	var ae = this.main.data.getProvider("activeevent");
	var evtalias = (Ext.isObject(ae)) ? ae.a : ae;

	var cb = Ext.bind(this.starteventCallback,this);
	var fcb = Ext.bind(this.starteventfailureCallback,this);

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='eventsgrid']"),
		message: 'Requesting Event...'
	});

	this.main.actions.perform({
          	cmd: 'request',
            	a: evtalias,
            	relationship: 'item',
            	callback: cb,
		failure: fcb,
            	scope: this,
            	component: sp.component
       	});
    },

    starteventfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);

	    me.main.actions.perform ({
		    cmd: 'removebusy',
		    component: me.main.view.down("*[name='eventsgrid']")
	    });

	    me.main.feedback.say({
		    title: 'Error',
		    message: 'Could not connect to server. Please try again.'
	    });
    },


    starteventCallback: function (rsp) {
	var me = (rsp.scope||this);
	
	me.main.actions.perform ({
	    cmd: 'removebusy',
	    component: me.main.view.down("*[name='eventsgrid']")
        });
	
	this.main.data.setProvider("{activeevent}",rsp.leaf);
	me.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'icdashboard',
            target: 'userinterface'
        });
    },

    /* -----------------------------------------------------------------------
    Checkout/Checkin:
    Checkout or Checkin an In-Competition test session.  When a test is checkout
    by a DCO it becomes locked so that no other DCO can start the test.  Checkin
    returns the test session to the pool of available test sessions.
    __________________________________________________________________________ */
    
    checkoutHandler: function (sp) {
        var ats = this.main.data.getProvider("activetestsession");
	var cb = Ext.bind(this.checkoutCallback,this);
	var fcb = Ext.bind(this.checkoutfailureCallback,this);

	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
		this.main.actions.perform({
        	    cmd: 'setbusy',
        	    component: this.main.view.down("*[name='activesessionsgrid']"), 
        	    message: 'Checking status...'
        	});

        	this.main.actions.perform({
			cmd: 'request',
			a: ats.ca,
			relationship: 'item',
			callback: cb,
			failure: fcb,
			scope: this,
			component: sp.component
		});
	} else {
		var status = ats.CheckedOut;
        	var dconame = this.main.entity.getFullName();
        	if ((!status)||(status==dconame)) {

 			var ocb = Ext.bind(this.checkoutofflineCallback,this,[sp.component],1);
        		// Verify selection with user
        		Ext.Msg.show({
        		    title: 'Checkout Test Session',
        		    message: 'This device is offline. Paperless cannot verify that the test session is not already checked out by another DCO.  Click OK to open the test session anyway.',
        		    buttons: Ext.Msg.OKCANCEL,
        		    fn: ocb
        		});
		} else {
			this.main.feedback.say({
        	        	title: 'Error',
        	        	message: 'This test session is checked out by another DCO.'
        	    	});
		}
	} 
    },
    
    checkoutofflineCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
	var ts = new Date();
        
        if (rsp==='ok') {
		var ats = me.main.data.getProvider("activetestsession");	
        	var dconame = me.main.entity.getFullName();
        	me.main.actions.perform({
        	    cmd: "put",
        	    component: cmp,
        	    objects: [ats],
        	    CheckedOut: dconame,
        	    CheckedOutTS: ts
        	});
        	me.startdcfHandler(cmp);
	}           

    },

    checkoutfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);
            me.main.actions.perform({
                cmd: 'removebusy',
                component: me.main.view.down("*[name='activesessionsgrid']")
            });
            

	    me.main.feedback.say({
		    title: 'Error',
		    message: 'Cannot connect to server. Please try again.'
	    });
    },

    checkoutCallback: function (rsp) {
	var me = (rsp.scope||this);
        var ts = new Date();
	var ats = rsp.leaf;

	me.main.actions.perform({
                cmd: 'removebusy',
                component: me.main.view.down("*[name='activesessionsgrid']")
            });
            

        var status = ats.CheckedOut;
        var dconame = me.main.entity.getFullName();
        if ((!status)||(status==dconame)) {
            me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ats],
                CheckedOut: dconame,
                CheckedOutTS: ts
            });
            me.startdcfHandler(rsp.component);
        } else {
            me.main.feedback.say({
                title: 'Error',
                message: 'This test session is checked out by another DCO.'
            });
        }
        
    },
    
    checkInActiveSessionHandler: function (sp) {
        var ts = new Date();
        
        var ats = this.main.data.getProvider("activetestsession");
        this.main.actions.perform({
            cmd: "put",
            component: this,
            objects: [ats],
            CheckedOut: false,
            CheckedOutTS: ts
        });

        this.gopreviousdashboardHandler();

    },
    
    /* -----------------------------------------------------------------------
    View History
    For In Competition Events open the site roster history for the athlete 
    __________________________________________________________________________ */
    
    viewrosterhistoryHandler: function (sp) {

	var asr = this.main.data.getProvider("activesiteroster");
	var cb = Ext.bind(this.viewrosterhistoryCallback,this);
	var fcb = Ext.bind(this.viewrosterhistoryfailureCallback,this);

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='siterostergrid']"),
		message: 'Getting roster history...'
	});

	this.main.actions.perform({
          	cmd: 'request',
            	a: asr.ca,
            	relationship: 'children',
            	callback: cb,
		failure: fcb,
            	scope: this,
            	component: sp.component
       	});
    },

    viewrosterhistoryfailureCallback: function (rsp) {
	    var me = (rsp.scope||this);

	    me.main.actions.perform({
		    cmd: 'removebusy',
		    component: me.main.view.down("*[name='siterostergrid']")
	    });

	    me.main.feedback.say({
		    title: 'Error',
		    message: 'Could not connect to server.  Please try again.'
	    });
    },

    viewrosterhistoryCallback: function (rsp) {
	var me = (rsp.scope||this);

	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='siterostergrid']")
	});

	me.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'siterosterhistoryform',
            target: 'userinterface'
        });
        

    },

    /* -----------------------------------------------------------------------
    Add Athlete:
    For In Competition Events opens a form so that the DCO can manually add an
    athletes information to the athlete queue.
    __________________________________________________________________________ */
    
    startAddAthleteHandler: function (sp) {
    
        var asr = this.main.data.getProvider("activesiteroster");
        
	if (asr.RosterHistory=='Completed') {
		this.main.feedback.say({
			title: 'Edit Athlete',
			message: 'This test session has been completed and the athlete information cannot be edited.'
		});
		return;
	}

        //var desc = this.main.data.getDescendants(asr.ca);
        //var arr = this.main.data.filter(desc, 'cid EQ athlete');
    
        //if (arr.length>0) {
	if (asr.testalias) {
            this.main.feedback.say({
                title: 'Edit Athlete',
                message: 'A test session has already been created for this athlete.  Use the active test session to edit athlete information.'
            });
            return;
        } else {
            this.main.actions.perform({
                cmd: 'request',
                source: 'addathletepanel',
                target: 'userinterface',
                provider: asr
             });
        }
         
    },
    
    searchAthletesHandler: function () {
    
	if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
        	var searchresults = this.main.view.down("*[name='selectathletegrid']");
    
        	var lastnamefield = this.main.view.down("*[name='athletesearchlastname']");
        	var lastname = (lastnamefield) ? lastnamefield.getValue() : '';
        	
        	var sportfield = this.main.view.down("*[name='athletesearchsport']");
        	var sport = (sportfield) ? sportfield.getValue() : '';
        	
        	var sp = {
        	    source: 'allathletes',
        	    relationship: 'descendants',
        	    $cid: 'athlete'
        	}
        	if (lastname) sp['ix1'] = lastname;
        	if (sport) sp['ix2'] = sport;

        	
        	searchresults.loadData(sp);
	} else {
		this.main.feedback.say({
			title: 'Search',
			message: 'This feature is only available online'
		})
	}
    
    },
    /* -----------------------------------------------------------------------
    Add Selected Athlete to Queue:
    For In Competition Events allows the user to add a new athlete to the athlete
    pool by selecting the athlete from a list of all athletes.
    __________________________________________________________________________ */
    
    addSelectedAthleteToQueueHandler: function (sp) {
     
	if (!this.main.environment.getOnline()||this.main.environment.get('safemode')){
		this.main.feedback.say({
			title: 'Error',
			message: 'This feature is only available online'
		})
		return;
	}   

	sp.component.setDisabled(true);

        var ae = this.main.data.getProvider("activeevent");
        
        // Find the selected athletes in the grid
        var agrid = this.main.util.get('selectathletegrid');
        var grid = agrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var atharray = [];
        
        // Add the selected athletes to an array
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].selectforpool) {
               atharray.push(data[i]);
           }
        }
        
        // Check that at least one athlete was selected
        if (atharray.length==0) {
            this.main.feedback.say({
                title: 'Add Selected to Pool',
                message: 'You must select at least one athlete to add to the athlete pool.'
            });
	    sp.component.setDisabled(false);
            return;
        }

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='selectathletegrid']"),
		message: 'Adding Athletes...'
	});

	var cb = Ext.bind(this.addtoqueueCallback,this,[atharray,sp.component],1);
	var fcb = Ext.bind(this.addtoqueuefailureCallback,this,[sp.component],1);

	this.main.actions.perform({
		cmd: 'request',
		a: ae.ca,
		relationship: 'descendants',
		callback: cb,
		failure: fcb,
		scope: this,
		component: sp.component
	});
    },

    addtoqueuefailureCallback: function (rsp,cmp) {
	var me = (rsp.scope||this);

	cmp.setDisabled(false);

	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='selectathletegrid']")
	});

	me.main.feedback.say({
		title: 'Error',
		message: 'Cannot retrieve event data.  Please try again.'
	});
    },

    addtoqueueCallback: function (rsp,atharray,cmp) {

	var me = (rsp.scope||this);
	var desc = rsp.desc; // active event
	var ae = rsp.leaf;
        
	
	// Check that the selected athletes are not already in the site roster
        var aearr = me.main.data.filter(desc, 'cid EQ athlete');
        var foundlist = '';
        for (var item in atharray) {
            var x = me.main.data.filter(aearr, 'PersonId EQ ' + atharray[item].PersonId);
            if (x.length>0) {
                foundlist = (foundlist=='') ? atharray[item].l : foundlist + ', ' + atharray[item].l;
            }
        }
        
        if (foundlist!='') {
            me.main.feedback.say({
                title: 'Add Selected to Pool',
                message: 'The following athletes are already in the site roster:<br>' + foundlist + '<br>Remove these athletes from the selection and try again.'
            });
	    cmp.setDisabled(false);

	    me.main.actions.perform({
		    cmd: 'removebusy',
		    component: me.main.view.down("*[name='selectathletegrid']")
	    });

            return;
        }

	var testsprocessed = 0;
	var completecallback = Ext.bind(me.completeaddtoqueueCallback,rsp.scope,[rsp,cmp],1);
        
	// Create a roster entry for the selected athletes
        //for (var i=0,l=atharray.length;i<l;i++){
	atharray.forEach(function(item,index,array) {

           // var ath = atharray[i];
           
           var cb = Ext.bind(me.addSelectedAthleteCallback,me,[item],1);
           
            // create the new site roster to add data to
            me.main.data.create({
                callback: cb,
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'siteroster',
                    pa: ae.a,
                    l: 'Roster Entry - ' + item.LastName,
                    t: 0,
                    RosterStatus: 'CREATED',
                    FirstName: item.FirstName,
                    LastName: item.LastName,
                    Sport: item.Sport,
                    Discipline: item.Discipline,
		    ix: index
                }]
            });

	    testsprocessed++;
	    if (testsprocessed===array.length){
		    completecallback();
	    }
        });
    },

    completeaddtoqueueCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);

	cmp.setDisabled(false);

	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='selectathletegrid']")
	});

        me.main.actions.perform({
            cmd: 'goto',
            redirectsource: 'athletelistform',
            target: 'userinterface'
        });
        
    },
    
    addSelectedAthleteCallback: function (rsp,ath) {
        var me = (rsp.scope||this);
        var ar = rsp.objects[0];
        
        // Link the athlete to the site roster
        me.main.data.createLink({
            parent: ar,
            child: ath
        });
        
        
    },
    
    /*------------------------------------------------------------------------
     * Remove from pool
     * For In Competition tests removes the athlete from the athlete pool
     * ______________________________________________________________________*/

    removefrompoolHandler: function (sp) {
	var ar = this.main.data.getProvider(sp.par.par.provider);

    	this.main.actions.perform({
            cmd: 'put',
            component: sp,
            objects: [ar],
            RosterStatus: 'DELETED'
        });

        
	this.main.data.move({
       	    component: sp,
       	    scope: this,
       	    source: ar,
       	    destination: this.main.environment.get("RECYCLEBIN")
       	});

	this.main.cache.invalidate(this.main.util.get('athletespool').sourcealias);
        this.main.util.get('athletespool').loadData();
    }, 
    /* -----------------------------------------------------------------------
    Add Athletes to Queue:
    For In Competition Events adds the manually added athlete to the site roster and
    creates new test sessions for the athlete.
    __________________________________________________________________________ */
    
    addAthleteManuallyHandler: function (sp) {
        
        var ae = this.main.data.getProvider("activeevent");
        var asr = this.main.data.getProvider("activesiteroster");
        
        if (!Ext.isDefined(asr.FirstName) || asr.FirstName=="") {
            this.main.feedback.say({
               title: 'Add Test Session',
               message: 'You must enter athletes first name.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.LastName) || asr.LastName=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must enter athletes last name.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.Sport) || asr.Sport=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must select a Sport.'
            });
            return;
        }
        
        if (!Ext.isDefined(asr.Discipline) || asr.Discipline=="") {
            this.main.feedback.say({
                title: 'Add Test Session',
                message: 'You must select a Discipline.'
            });
            return;
        }

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='addathleteform']"),
		message: 'Getting event data...'
	});

	this.main.actions.perform({
          	cmd: 'request',
            	a: ae.ca,
            	relationship: 'children',
            	callback: this.requesteventCallback,
		failure: this.requesteventfailureCallback,
            	scope: this,
            	component: sp.component
       	});
    },

   requesteventfailureCallback: function (rsp) {
	var me = (rsp.scope||this);
	var ae = rsp.a;

	Ext.callback(me.requesteventCallback, me, [{
		leaf: me.main.data.getChildren(ae),
		scope: me,
		component: rsp.component
	}]);
   },

   requesteventCallback: function (rsp) {

        var me = (rsp.scope||this);
        var ts = new Date();
	var ae = rsp.leaf;
	//var ae = rsp.desc;
	if (!Ext.isDefined(ae)) {
		me.main.feedback.say({
			title: 'Error Adding Test Session',
			message: 'Cannot find data for this event. Please try again.'
		});
		me.main.actions.perform({
			cmd: 'removebusy',
			component: me.main.view.down("*[name='addathleteform']")
		});
		return;
	}

        var asr = me.main.data.getProvider("activesiteroster");
	var authorities = me.getsportauthorities(ae,asr.Sport);
	
        // Update the Site roster entry for the athlete
        me.main.actions.perform({
            cmd: "put",
            component: rsp.component,
            objects: [asr],
            RosterStatus: 'OPENED',
            RosterHistory: 'Checked In',
            timestamp: ts,
	    l: 'Roster Entry - ' + asr.LastName
        });
       
       // Create a ledger entry to check athlete into site roster
        me.main.data.create({
            component: rsp.component,
            scope: me,
            data: [{
                cid: 'ledgerentry',
                l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: asr.a,
                t: 0,
                Timestamp: ts,
                RosterHistory: 'Checked In'
            }]
        });
        
	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='addathleteform']")
	});

        var cb = Ext.bind(me.addAthleteManuallyCallback,me);
        
        // Create the test session
        me.main.data.create({
            callback: cb,
            component: rsp.component,
            scope: me,
            data: [{
                cid: 'testsession',
                l: 'Test Session ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: ae.a,
                t: 0,
                ArrivalDate: ts,
                ArrivalTime: ts,
                EventID: ae.EventID,
                TestingAuthority: authorities.testingauthority,
                ResultsManagementAuthority: authorities.resultsauthority,
		TestingAuthorityLongName: authorities.testingauthoritylongname,
		ResultsAuthorityLongName: authorities.resultsauthoritylongname,
		SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
		CollectionAuthority: 'USADA',
                SiteID: ae.SiteId,
		MissionCode: ae.MissionOrder || '',
                EventType: 'IC',
                designation: 'event',
                Urine: true,
                UrineType: 'full',
                Status: 'ASSIGNED',
                CheckedOut: false,
                Sport: asr.Sport,
                Sport_caption: asr.Sport_caption,
                Discipline: asr.Discipline,
                Discipline_caption: asr.Discipline_caption
            }]
        });
    },
    
    addAthleteManuallyCallback: function (rsp) {
        
        var me = (rsp.scope||this);
        var ts = rsp.objects[0];
        var asr = me.main.data.getProvider("activesiteroster");
        
	// Add the test session alias to the roster so that
	// the test can be removed if the athlete is removed
	// from the site roster
	me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [asr],
                testalias: ts.a
            });

        var cb = Ext.bind(me.linkAthleteManuallyCallback,me,[ts],1);

        // Create an athlete object for this test
        me.main.data.create({
            callback: cb,
            component: rsp.component,
            scope: me,
            data: [{
                cid: 'athlete',
                l: asr.LastName + ' ' + asr.FirstName,
                pa: ts.a,
                t: 0,
                FirstName: asr.FirstName,
                LastName: asr.LastName,
                Sport: asr.Sport_caption,
                Discipline: asr.Discipline_caption
            }]
        });
        
    },
        
    linkAthleteManuallyCallback: function (rsp,ts) {
    
        var me = (rsp.scope||this);
        var asr = me.main.data.getProvider("activesiteroster");

        //debugger;
        // Link the athlete to the site roster
        me.main.data.createLink({
            parent: asr,
            child: rsp.objects[0]
        });
        
        this.gopreviousdashboardHandler();
        
    },
    
    /* ----------------------------------------------------------------------
     * Get Sport Authorities
     * Gets the Testing Authority and Results Management Authorities from the
     * sport authorities object in the active event
     * ____________________________________________________________________*/

     getsportauthorities: function (ae,sport) {

       	var desc = this.main.data.getDescendants(ae.a);

	// Get the Testing and Results Authorities from the sportauthority object
	var ra = 'USADA';
	var ta = 'USADA';
	var raln = 'United States Anti-Doping Agency';
	var taln = 'United States Anti-Doping Agency';
	var saarr = this.main.data.filter(desc,"cid EQ sportauthority");
	for (var i=0,l=saarr.length;i<l;i++) {
		if (saarr[i].SportId == sport) {
			ra = saarr[i].ResultsAuthorityAbbreviation;
			ta = saarr[i].TestAuthorityAbbreviation;
			taln = saarr[i].TestingAuthorityLongName;
			raln = saarr[i].ResultsAuthorityLongName;
		}
	}

	return {resultsauthority:ra,resultsauthoritylongname:raln,testingauthority:ta,testingauthoritylongname:taln};

    },
    
    /* -----------------------------------------------------------------------
    Add Athletes to Roster:
    For In Competition Events adds the selected athletes to the site roster and
    creates new test sessions for the athletes.
    __________________________________________________________________________ */
    
    addAthletesToRosterHandler: function (sp) {
    
	sp.component.setDisabled(true);

	if (!this.main.environment.getOnline()||this.main.environment.get('safemode')){
		this.main.feedback.say({
			title: 'Error',
			message: 'This feature is only available online'
		})
		sp.component.setDisabled(false);
		return;
	}

	// Find the selected athletes in the grid
        var agrid = this.main.util.get('athletespool');
        var grid = agrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var atharray = [];
        
        // Add the selected athletes to an array
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].selectedathlete) {
               atharray.push(data[i]);
           }
        }
        
	// if nothing selected prompt user and exit
        if (atharray.length==0) {
            this.main.feedback.say({
                title: 'Error',
                message: 'You must select at least one athlete from the athlete pool.'
            });
	    sp.component.setDisabled(false);
            return;
        }

	this.main.actions.perform({
		cmd: 'setbusy',
		component: this.main.view.down("*[name='athletespool']"),
		message: 'Creating Test Sessions...'
	});

        var ae = this.main.data.getProvider("activeevent");
	var cb = Ext.bind(this.getactiveeventCallback,this,[atharray,sp.component],1);
	var fcb = Ext.bind(this.getactiveeventfailureCallback,this,[sp.component],1);

	this.main.actions.perform({
		cmd: 'request',
		a: ae.ca,
		relationship: 'descendants',
		callback: cb,
		failure: fcb,
		scope: this,
		component: sp.component
	});
    },

    getactiveeventfailureCallback: function (rsp,cmp) {
	var me = (rsp.scope||this);

	cmp.setDisabled(false);

	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='athletespool']")
	});

	me.main.feedback.say({
		title: 'Error',
		message: 'Cannot retrieve event data.  Please try again.'
	});
    },

    getactiveeventCallback: function (rsp,atharray,cmp) {

	var me = (rsp.scope||this);
	var desc = rsp.desc; // active event
	var ae = rsp.leaf;
	var testsprocessed = 0;
	var completecallback = Ext.bind(me.completeaddtestsessionCallback,rsp.scope,[rsp,cmp],1);
	
	atharray.forEach(function(item,index,array) {
	    var arl = me.main.data.filter(desc, 'ca EQ ' + item.ca); // active roster array
	    var ts = new Date();
	    var ar,aa;

	    // Check that the roster was found
	    if (Ext.isEmpty(arl)) {
		    me.main.feedback.say({
			    title: 'Error',
			    message: 'Cannot find the roster entry for this athlete.'
		    });
		    return;
	    } else {
		    ar = arl[0]; // active roster
	    }

	    var aal = me.main.data.getChildren(ar.a); // athlete array
            // Check that athlete is linked to roster entry
            if (Ext.isEmpty(aal)) {
		    me.main.feedback.say({
			    title: 'Error',
			    message: 'Roster entry not associated with a valid athlete.'
		    });
                return;
            } else {
		    aa = aal[0]; // active athlete
	    }
            
	    // Update the Site roster entry for the athlete
            me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                RosterStatus: 'OPENED',
                RosterHistory: 'Checked In',
                timestamp: ts
            });
           
           // Create a ledger entry to check athlete into site roster
            me.main.data.create({
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'ledgerentry',
                    l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ar.ca,
                    t: 0,
                    Timestamp: ts,
                    RosterHistory: 'Checked In',
		    ix: index
                }]
            });
        
	    // Lookup the id values to write to the test session
            var sportval = aa.Sport || '';
            var sportId = '';
            if (sportval) sportId = me.main.localization.reverselookupValue('sport','Sport',sportval,'SportId');
	    if (!Number.isInteger(sportId)) sportId = '';
            
            var disciplineval = aa.Discipline || '';
            var disciplineId = '';
            if (disciplineval) disciplineId = me.main.localization.reverselookupValue('discipline','Discipline',disciplineval,'DisciplineId');
	    if (!Number.isInteger(disciplineId)) disciplineId = '';
            
	    var authorities = me.getsportauthorities(ae,sportId);
	    
	    var cb = Ext.bind(me.addTestSessionCallback,me,[aa,ar],1);
        
            // Create a new test session
            me.main.data.create({
                callback: cb,
                component: rsp.component,
                scope: me,
                data: [{
                    cid: 'testsession',
                    l: 'Test Session ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                    pa: ae.a,
                    t: 0,
                    ArrivalDate: ts,
                    ArrivalTime: ts,
                    EventID: ae.EventID,
                    SiteID: ae.SiteId,
		    MissionCode: ae.MissionOrder || '',
                    EventType:'IC',
                    designation: 'event',
                    Urine: true,
                    UrineType: 'full',
                    Status: 'ASSIGNED',
                    Sport: sportId,
                    Sport_caption: sportval,
                    Discipline: disciplineId,
                    Discipline_caption: disciplineval,
                    TestingAuthority: authorities.testingauthority,
                    ResultsManagementAuthority: authorities.resultsauthority,
	            TestingAuthorityLongName: authorities.testingauthoritylongname,
	            ResultsAuthorityLongName: authorities.resultsauthoritylongname,
	            SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
	            CollectionAuthority: 'USADA',
	            AddressType: aa.AddressType||'',
                    ResidenceAddr1: aa.Addr1||'',
                    ResidenceCity: aa.City||'',
                    ResidenceState: aa.State||'',
                    ResidenceZip: aa.Zip||'',
                    ResidenceCountry_caption: aa.Country||'',
   		    CheckedOut: false,
		    ix: index
                }]
            });

     	    // when all tests are processed call the complete callback
	    testsprocessed++;
	    if (testsprocessed===array.length){
		    completecallback();
	    }
           
        });
    },

    completeaddtestsessionCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);

	// Enable the submit button
	cmp.setDisabled(false);

	// Remove busy indicator off of grid
	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='athletespool']")
	});

        me.gopreviousdashboardHandler();
    },
    
    addTestSessionCallback: function (rsp,aa,ar) {
        
        var me = (rsp.scope||this);
        var ts = rsp.objects[0];
        
	// Add the test session alias to the roster so that
	// the test can be removed if the athlete is removed
	// from the site roster
	me.main.actions.perform({
                cmd: "put",
                component: rsp.component,
                objects: [ar],
                testalias: ts.a
            });

	// Link the athlete to the new test session record
       	me.main.data.createLink({
       	    parent: ts,
       	    child: aa
       	});
	
        //var cb = Ext.bind(me.linkTestSessionCallback,me,[ts,ae],1);

        //// Request the athele object so we can link to this test session
	//me.main.actions.perform({
        //    cmd: 'request',
        //    a: aa.ca,
        //    relationship: 'descendants',
        //    callback: cb,
        //    scope: me,
        //    component: rsp.component
        //});
        
    },
        
    linkTestSessionCallback: function (rsp,ts,ae) {
    
        var me = (rsp.scope||this);
	var aa = rsp.leaf;

	if (!Ext.isDefined(aa)) {
		me.main.feedback.say({
			title: 'Error',
			message: 'Cannot find athlete record to link to test session.'
		});
		return;
	} 
       	// Link the athlete to the new test session record
       	me.main.data.createLink({
       	    parent: ts,
       	    child: aa
       	});
	
        
        // Lookup the id values to write to the test session
        var sportval = aa.Sport || '';
        var sportId = '';
        if (sportval) sportId = me.main.localization.reverselookupValue('sport','Sport',sportval,'SportId');
        
        var disciplineval = aa.Discipline || '';
        var disciplineId = '';
        if (disciplineval) disciplineId = me.main.localization.reverselookupValue('discipline','Discipline',disciplineval,'DisciplineId');
        
	var authorities = me.getsportauthorities(ae,sportId);
	
        // Add athlete info to test session if it exists.
        this.main.actions.perform({
            cmd: "put",
            component: rsp,
            objects: [ts],
            Sport: sportId,
            Sport_caption: sportval,
            Discipline: disciplineId,
            Discipline_caption: disciplineval,
            TestingAuthority: authorities.testingauthority,
            ResultsManagementAuthority: authorities.resultsauthority,
	    TestingAuthorityLongName: authorities.testingauthoritylongname,
	    ResultsAuthorityLongName: authorities.resultsauthoritylongname,
	    SampleCollectionAuthorityLongName: 'United States Anti-Doping Agency',
	    CollectionAuthority: 'USADA',
	    AddressType: aa.AddressType||'',
            ResidenceAddr1: aa.Addr1||'',
            ResidenceCity: aa.City||'',
            ResidenceState: aa.State||'',
            ResidenceZip: aa.Zip||'',
            ResidenceCountry_caption: aa.Country||''
        });
        
    },

    /* -------------------------------------------------------------
     * Check active ic test
     * Checks on the status of the ic test associated with the roster
     * entry that the user is trying to delete.  If the test is checked
     * out, has been started, or has been completed then the user is
     * unable to delete the roster entry; otherwise the roster entry
     * is removed and the associated test session is deleted
     * ____________________________________________________________*/
    
    checkactiveictestHandler: function (sp) {
	// Get the test session associated with this roster entry
	var tsa = sp.value.data.testalias;
	
	if (Ext.isEmpty(tsa)) {
		// No test session associated with roster - skip status check and just remove it
		Ext.callback(sp.callback,this,[sp.value,sp.idx]);
	} else {
		var cb = Ext.bind(this.checkactiveictestCallback,this,[sp.callback,sp.value,sp.idx],1);
		var fcb = Ext.bind(this.checkactiveictestfailureCallback,this);

		if (this.main.environment.getOnline()&&!this.main.environment.get('safemode')){
			this.main.actions.perform({
				cmd: 'setbusy',
				component: this.main.view.down("*[name='siterostergrid']"),
				message: 'Checking test session status...'
			});

			this.main.actions.perform({
        		        cmd: 'request',
        		        a:tsa, 
        		        relationship: 'item',
        		        callback: cb,
				failure: fcb,
        		        scope: sp.scope,
        		        component: sp.cmp
                	});
		} else {
			this.main.feedback.say({
				title: 'Error',
				message: 'Cannot delete roster entry offline.  Paperless must be online in order to verify the status of the test session associated with this roster entry.'
			});
			return;
		}
	}

    },

    checkactiveictestfailureCallback: function (rsp) {
	var me = (rsp.scope||this);

	this.main.feedback.say({
		title: 'Error',
		message: 'Cannot verify test session status.  Please try again.'
	});

	this.main.actions.perform({
		cmd: 'removebusy',
		component: this.main.view.down("*[name='siterostergrid']")		
	});	

    },

    checkactiveictestCallback: function (rsp,cb,value,idx) {
	// Check that the test session associated with the roster entry hasn't been 
	// completed, started, or checked out before removing the test and roster entry
	var msg = '';

	this.main.actions.perform({
		cmd: 'removebusy',
		component: this.main.view.down("*[name='siterostergrid']")		
	});

	if (rsp.leaf._Status=='COMPLETED'){
	        // Test session has been completed
	        msg ='The test session associated with this roster entry has been completed.'; 
	} else if (rsp.leaf._Status=='OPENED') {
		// Test session has been started
		msg = 'The test session associated with this roster entry has already been started.';
	} else if (rsp.leaf.CheckedOut!=false) {
		// test session is checked out to someone
		msg = 'The test session associated with this roster entry is currently in use.';
	}

	if (msg!='') {
		this.main.feedback.say({
			title: 'Cannot Delete Roster Entry',
			message: msg
		});
		return false;
	}

	Ext.callback(cb,rsp.scope,[value,idx]);

    }, 

    removeactiveictestHandler: function (sp) {
	var tsa = sp.value.testalias;
	
	if (!Ext.isEmpty(tsa)) {
		this.main.actions.perform({
			cmd: 'setbusy',
			component: this.main.view.down("*[name='siterostergrid']"),
			message: 'Removing test session...'
		});

		this.main.actions.perform({
        	        cmd: 'request',
        	        a:tsa, 
        	        relationship: 'item',
        	        callback: this.removeactiveictestCallback,
			failure: this.removeactiveictestfailureCallback,
        	        scope: this,
        	        component: sp.cmp
        	 });
	}

    },

    removeactiveictestfailureCallback: function (rsp) {
	var me = (rsp.scope||this);

	var cb = Ext.bind(me.removeactiveictestCallback,me);
	Ext.callback(cb,me,[{
	    leaf:  me.main.data.getSource(rsp.a),
	    scope: me,
	    component: rsp.component
	}]);

    },

    removeactiveictestCallback: function (rsp) {
	var me = (rsp.scope||this);

	if (Ext.isDefined(rsp.leaf)){
		me.main.data.move({
       		    component: rsp.component,
       		    scope: me,
       		    source: rsp.leaf,
       		    destination: me.main.environment.get("RECYCLEBIN")
       		});
	} else {
		me.main.feedback.say({
			title: 'Error',
			message: 'Could not remove the test session associated with this roster entry'
		});
	}

	me.main.actions.perform({
		cmd: 'removebusy',
		component: me.main.view.down("*[name='siterostergrid']")
	});
	

        me.main.util.get('activesessionsgrid').loadData();


    },
    /* -----------------------------------------------------------------------
    Update person roster status:
    Toggles the roster status for the active support person and creates a new 
    roster entry.
    __________________________________________________________________________ */
    
    updatePersonRosterStatusHandler: function (sp) {
    
        var ap = this.main.data.getProvider("activeperson");
        
        var newstatus = (ap.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var cb = Ext.bind(this.updatePersonStatusCallback,this,sp.component,1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this person to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    updatePersonStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var ap = me.main.data.getProvider("activeperson");
        
        if (rsp==='ok') {
            me.doUpdateRosterStatus(ap,cmp);
        }
        
    },
    
    /* -----------------------------------------------------------------------
    Update athlete roster status:
    Updates the site roster status for the athlete: toggles between Checked In/
    Checked Out; adds a new roster entry.
    __________________________________________________________________________ */
    
    updateAthleteRosterStatusHandler: function (sp) {
        var ar = this.main.data.getProvider("activesiteroster");
        
	if (ar.RosterHistory=='Completed') {
	    this.main.feedback.say({
                title: 'Update Site Roster Status',
                message:'This test session has been completed.'
            });
            return;
	}

        var newstatus = (ar.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        var tgrid = this.main.util.get('siterostergrid');
        var grid = tgrid.down("grid");

        
        var cb = Ext.bind(this.updateAthleteStatusCallback,this,[tgrid],1);
        Ext.Msg.show({
            title: 'Update Site Roster Status',
            message: 'Update the Site Roster status for this athlete to ' + newstatus + '?',
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
//        this.doUpdateRosterStatus(ar);
    },
    
    updateAthleteStatusCallback: function (rsp,cmp) {
        var me = (rsp.scope||this);
        var asr = me.main.data.getProvider("activesiteroster");
        
        if (rsp==='ok') {
           me.doUpdateRosterStatus(asr,cmp);
        }
    },
    
    doUpdateRosterStatus: function (rsp,cmp) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        var newstatus = (rsp.RosterHistory=='Checked In') ? 'Checked Out' : 'Checked In';
        
        me.main.actions.perform({
            cmd: "put",
            component: me,
            objects: [rsp],
            RosterHistory: newstatus
        });
       
        //Create a ledger entry for this person
        me.main.data.create({
            component: me,
            scope: me,
            data: [{
                cid: 'ledgerentry',
                l:  'Ledger Entry ' + Ext.Date.format(ts,'m-d-Y h:i A'),
                pa: rsp.a,
                t: 0,
                Timestamp: ts,
                RosterHistory: newstatus
            }]
        });
        
        var arr = me.main.render.getHistory();
        
        for (var i=arr.length-1,l=0;i>l;i--){
            //var item = arr.pop();
	    var item = arr[i];
            var src = item.source||item.redirectsource;
            me.main.actions.perform(item);
            break;
        }
           
        //        cmp.loadData();
        
//        me.main.actions.perform({
//            cmd: 'request',
//            source: 'icdashboard',
//            target: 'userinterface'
//        });
    },
    
    updateAthleteRosterEntourageHandler: function (sp) {
    
    },
    
    /* -----------------------------------------------------------------------
    Offer Assignment DCO:
    Enables RTL user to offer a test session assignment to the selected DCO:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to DCO; Updates test session status.
    __________________________________________________________________________ */
    
    offerassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferDCO');
        var aDCO = cbox.getRawValue();
        var dcoID = cbox.findRecordByDisplay(aDCO).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'DCO':aDCO,'DCOId':dcoID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Offer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.doofferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Offer Assignment',
            message: 'The following assignments will be offered to ' + aDCO + ':<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    doofferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var DCOID = tdata[0].DCOId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.doofferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.doofferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    dcoid: DCOID,
//                    statusid: "9",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'icdashboard',
                target: 'userinterface'
            });
           
        }
    },
    
    doofferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pdco = 'dco.' + tdata[0].DCOId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                _Status: 'OFFERED',
                OfferedDt: ts,
                pa: pdco
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Transfer Assignment DCO:
    Enables RTL user to transfer a test session assignment to the selected region:
    Checks AssignmentAPI for the test session status; Writes status update to
    AssignmentAPI; Reparents test session to region; Updates test session status.
    __________________________________________________________________________ */
    
    transferassignmentdcoHandler: function (sp) {
         
        // Get the DCO from the combobox
        var cbox = this.main.util.get('transferRegion');
        var aRegion = cbox.getRawValue();
        var regionID = cbox.findRecordByDisplay(aRegion).data.value;
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('rtlunassignedgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [{'Region':aRegion,'RegionId':regionID}];
        var tlist='';
        
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].TransferTestSession) {
               tarray.push(data[i]);
           }
        }
        
        if (tarray.length==1) {
            this.main.feedback.say({
                title: 'Transfer Assignment',
                message: 'You must select at least one test session assignment.'
            });
            return;
        }
        
        for (var i=1,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        var cb = Ext.bind(this.dotransferassignmentcheck,this,[tarray],1);
        // Verify selection with user
        Ext.Msg.show({
            title: 'Transfer Assignment',
            message: 'The following assignments will be transferred to ' + aRegion + ' Region:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
        
    },
    
    dotransferassignmentcheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        if (rsp==='ok') {
//            debugger;

            var RegionID = tdata[0].RegionId;

            for (var i=1,l=tdata.length;i<l;i++){
           
                var tsid = tdata[i].TSID;
                // bind callback function, attaching the test session object as
                // an additional argument
                var cb = Ext.bind(me.dotransferassignmentCallback,me,tdata[i],1);
           
                // TESTING
                var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"9","status":"Offered","statusdate":"today"}]};
                me.dotransferassignmentCallback(rval,[tdata[0],tdata[i]]);
           
                // write test session assignment to API
//                me.main.actions.perform({
//                    cmd: "task",
//                    n: "testsessionassignment",
//                    regionid: RegionID,
//                    statusid: "11",
//                    testsessionid: tsid,
//                    callback: cb,
//                    scope: me
//                });
            }
           
            me.main.actions.perform({
                cmd: 'request',
                source: 'rtlassignmentsdash',
                target: 'userinterface'
            });
           
        }
    },
    
    dotransferassignmentCallback: function (rsp,tdata) {
        var me = (rsp.scope||this);
        
        var ts = new Date();
        
        // Update the status and reset the parent
        if (rsp.type=="success") {
            var pregion = 'region.' + tdata[0].RegionId;
            me.main.actions.perform({
                cmd: "put",
                component: me,
                objects: [tdata[1]],
                _Status: 'UNASSIGNED',
                pa: pregion
            });
        }

    },
    
    /* -----------------------------------------------------------------------
    Accept Assignment:
    Enables DCO user to accept one or more offered assignments: Checks 
    AssignmentAPI for test session status; Writes status update to AssignmentAPI;
    Changes status of test session.
    __________________________________________________________________________ */
    
    acceptassignmentsHandler: function (sp) {
        
        // Find the selected test sessions in the grid
        var tgrid = this.main.util.get('offeredassignmentsgrid');
        var grid = tgrid.down("grid");
        var store = grid.getStore();
        var data = store.proxy.data;
        var tarray = [];
        var tlist='';
        
        // Find the selected items in the grid
        for (var i=0,l=data.length;i<l;i++) {
           if (data[i].SelectedTestSession) {
               tarray.push(data[i]);
           }
        }
        
        // Check that something is selected
        if (tarray.length==0) {
            this.main.feedback.say({
                title: 'Accept Assignment',
                message: 'You must select at least one test session assignment'
            });
            return;
        }
        
        // format for verification
        for (var i=0,l=tarray.length;i<l;i++) {
            tlist = (tlist=='') ? tarray[i].AthletePrintedName : tlist + '<br>' + tarray[i].AthletePrintedName;
        }
        
        // Include the selected test sessions in the callback
        var cb = Ext.bind(this.acceptassignmentscheck,this,[tarray],1);
        
        // Verify selection with user
        Ext.Msg.show({
            title: 'Accept Assignments',
            message: 'The following assignments will be accepted:<br>' + tlist,
            buttons: Ext.Msg.OKCANCEL,
            fn: cb
        });
    },
    
    acceptassignmentscheck: function (rsp,tdata) {
        var me = (rsp.scope||this);
    
        if (rsp==='ok') {
            for (var i=0,l=tdata.length;i<l;i++){
                var tsid = tdata[i].TSID;
                me.doacceptassignmentHandler(tdata[i]);
            }
        }
        me.assignmentsdashboardHandler();
    },
    
    acceptassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        this.doacceptassignmentHandler(aa);
        this.assignmentsdashboardHandler();
    },
    
    doacceptassignmentHandler: function (rsp) {
    
        var me = (rsp.scope||this);

        // Dummy up return value until assignment is working on server
//        var rval = {"applicationid":"testsessionassignment","type":"success","message":"","data":[{"testsessionid":"8675309","dcoid":"1","statusid":"1","status":"Accepted","statusdate":"today"}]};
//        this.acceptassignmentCallback(rval,[rsp]);

//        debugger;
        
        // DISABLED UNTIL ASSIGNMENT TASK IS WORKING ON SERVER
        var cb = Ext.bind(me.acceptassignmentCallback,me,[rsp],1);
        
        // Write assignment status to API
        this.main.actions.perform({
            cmd: "task",
            n: "testsessionassignment",
            action: '/api/write',
            testsessionid: rsp.TSID,
            dcoid: 6||this.entity.getUser().DCOID,
            statusid: 1,
            callback: cb,
            scope: this
        });
    },
    
    acceptassignmentCallback: function (rsp, atest) {
        var me = (rsp.scope||this);
        
//        debugger;
           
        switch (rsp.success) {
            case "warning":
            case "success":
                me.main.actions.perform({
                    cmd: "put",
                    component: me,
                    objects: [atest[0]],
                    _Status: 'ASSIGNED'
                });
                break;
           case "error":
               var msgtitle = 'Error';
               var msgmessage = 'An error has occurred accepting assignment.';
               if ((Ext.isArray(rsp.events)) && (rsp.events.length>0)) {
                   msgtitle = rsp.events[0].Message;
                   msgmessage = rsp.events[0].message;
               }
               this.main.feedback.say({
                   title: msgtitle,
                   message: msgmessage
               });
               break;
        }
    },
    
    assignmentsdashboardHandler: function (sp) {

        this.main.actions.perform({
            cmd: 'request',
            source: 'assignmentsdash',
            target: 'userinterface'
        });
    },
    
    declineassignmentHandler: function (sp) {
        var aa = this.main.data.getProvider("activeassignment");
        
    },

    /* -----------------------------------------------------------------------
    Load Whereabouts:
    Opens a new browser window and displays whereabouts info for the selected
    athlete.
    __________________________________________________________________________ */
    
    loadwhereaboutsHandler: function () {
    
        this.main.actions.perform({
            cmd: "task",
            n: "whereabouts",
            dcoid: 6||this.entity.getUser().DCOID,
            callback: this.loadwhereaboutsCallback,
            scope: this
        });
    
    },
    
    loadwhereaboutsCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //TODO:  need to add athlete id to this url per documentation.
        var url = "https://athlete-admin-staging.usada.org/Whereabouts/dco/forathlete?logintoken=" + rsp.events[0].token + "&athleteid=55299";
        
        //Open in a new browser window in Safari.
        window.open(url, "_system");

    },
    
    
    
     /* -----------------------------------------------------------------------
    Business Rules.  Checks a test session to see if it is complete.
    __________________________________________________________________________ */
    
    
    checktestsessionHandler: function (sp) {
    
        //This function is called after rule's component (DCF tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the DCF section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var ts = this.main.data.getProvider("{activetestsession}");
            var ath = this.main.data.getProvider("{activetestsession > athlete[0]}");

            
            //DCF
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('dcor'),container:'dcfLandingForm'},obj));
            
            
            
            //Notification Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('notification'),container: 'dcornot',completed:[ath.athletenameFieldset_completed,ts.notificationFieldset_completed,ts.chaperoneFS_completed,ts.notificationPart4_completed]},obj));
            
            
            //Part 1 (Notification)
this.checkSection(Ext.apply({title: this.main.localization.translate("notification"),container: "notificationPart1",managers: ["athletenameFieldset","notificationFieldset","chaperoneFS"],required: true},obj));
            
            //Part 2 (Regulations)
            this.checkSection(Ext.apply({title: this.main.localization.translate("regulations"),container: "notificationPart2"},obj));
            
            //Part 3 (Notification Letter)
            this.checkSection(Ext.apply({title: this.main.localization.translate("notificationletter"),container: "notificationPart3"},obj));
           
            //Part 4 (Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("signature"), container: "notificationPart4", managers: ["notificationPart4"],required:true},obj));
    

            //Athlete Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('athleteinfotitle'),container: 'dcorai',completed:[ts.athleteinfoPart1_completed,ts.identificationFieldset_completed,ath.dobFieldset_completed,ath.nationalityFS_completed,ts.residenceAddressFS_completed,ts.mailingFS_completed]},obj));
            
            
            //Part 1 (Arrival)
            this.checkSection(Ext.apply({title: this.main.localization.translate("arrival"), container: "athleteinfoPart1", managers: ["athleteinfoPart1"],required:true},obj));
            
            //Part 2 (Identification)
            this.checkSection(Ext.apply({title: this.main.localization.translate("identification"),container: "athleteinfoPart2", managers: ["identificationFieldset","dobFieldset","nationalityFS"],required:true},obj));
        
            //Part 3 (Residence Address)
            this.checkSection(Ext.apply({title: this.main.localization.translate("residenceaddress"),container: "athleteInfoPart3", managers: ["residenceAddressFS"],required:true},obj));
            
            //Part 4 (Mailing Address)
            this.checkSection(Ext.apply({title: this.main.localization.translate("mailingaddress"),container: "athleteInfoPart4", managers: ["mailingFS"],required:true},obj));
            
            //Part 5 (Support Personnel)
            this.checkSection(Ext.apply({title: this.main.localization.translate("athleterepresentative"),container: "athleteInfoPart5"},obj));


            //Lab/Sample Info Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('labsampleinfotitle'),container: 'dcorlab',completed:[ts.labSampleInfoPart1_completed,ath.genderFS_completed,ts.labSampleInfoPart3_completed]},obj));
            
            //Part 1 (Competition)
            this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "labSampleInfoPart1",managers: ["labSampleInfoPart1","genderFS"],required: true},obj));
            
            //Part 2 (Tests)
            this.checkSection(Ext.apply({title: this.main.localization.translate("tests"),container: "labSampleInfoPart2"},obj));
            
            //Part 3 (Site)
            this.checkSection(Ext.apply({title: this.main.localization.translate("site"),container: "labSampleInfoPart3",managers: ["labSampleInfoPart3"],required: true},obj));


            //Blood Sample Form
            //-------------------------------
            
            
            //Need to loop over bloodsamples.
            var bcomplete = true;
            var bfound = false;
            var bsamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ bloodsample");
            for (var i=0,l=bsamples.length;i<l;i++) {
                bfound = true;
                var b = bsamples[i];
                if (b.BloodSampleInfo_completed) {
                } else {
                    bcomplete = false;
                }
            }
            
            //Recheck Blood Samples list.
            //Might need to move this into a rule that only works when Blood data is being added.
            //var dvw = sp.component.down("xdataview[name='BloodSamplesList']");
            //if (dvw) dvw.refresh();
	    var dvw = sp.component.down("xgrid[name='bsamplesgrid']");
	    if (dvw) dvw.loadData();

            if (ts.Blood!=true){
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl",disabled: true},obj));
            } else {
                if (bfound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl",completed: [bcomplete,ts.BloodQuestionsSet1_completed,ts.BloodQuestionsSet2_completed,ts.BloodQuestionsSet3_completed,ts.BloodQuestionsSet4_completed,ts.BloodSampleBCOSignature_completed,ts.BloodSampleDCOSignature_completed]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("bloodsample"),container: "dcorbl", completed: [false]},obj));
                }
            }
            
            //Competition
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "BloodQuestionsSet1",managers:["BloodQuestionsSet1"],required: true},obj));
            } else {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("competition"),container: "BloodQuestionsSet1",required: true},obj));
            }
            
            //Altitude
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("altitudetitle"),container: "BloodQuestionsSet2",managers:["BloodQuestionsSet2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("altitudetitle"),container: "BloodQuestionsSet2",required: true},obj));
            }
                  
            //Extreme Conditions
            if (ts.Blood) {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("extrmeconditions"), container: "BloodQuestionsSet3", managers:["BloodQuestionsSet3"], required: true},obj));
            } else {
                  this.checkSection(Ext.apply({title: this.main.localization.translate("extrmeconditions"), container: "BloodQuestionsSet3",requried: true},obj));
            }
 
            //Blood loss/transfusion
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bloodlosstransfusion"), container: "BloodQuestionsSet4", managers:["BloodQuestionsSet4"], required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bloodlosstransfusion"), container: "BloodQuestionsSet4",requried: true},obj));
            }
                  
            //Part 3 (Sample Info)
            if (bfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"),container: "BloodSampleInfo", completed: bcomplete},obj));
            } else {
//                this.checkSection(Ext.apply({title: this.main.localization.translate("sampleinfo"),container: "BloodSampleInfo",required: true},obj));
                  this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"), container: "BloodSampleInfo", completed: false},obj));
            }
            
            //Part 4 (BCO Signature)
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "BloodSampleBCOSignature",managers:["BloodSampleBCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "BloodSampleBCOSignature",required: true},obj));
            }
            
            //Part 5 (DCO Signature)
            if (ts.Blood) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "BloodSampleDCOSignature",managers:["BloodSampleDCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "BloodSampleDCOSignature",required: true},obj));
            }
            
            //DBS Sample Form
            //-------------------------------
            
            
            //Need to loop over dried blood spot samples.
            var dbscomplete = true;
            var dbsfound = false;
            var dbssamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ dbssample");
            for (var i=0,l=dbssamples.length;i<l;i++) {
                dbsfound = true;
                var b = dbssamples[i];
                if (b.DBSSampleInfo_completed) {
                } else {
                    dbscomplete = false;
                }
            }
            
            //Recheck DBS Samples list.
            var dbs = sp.component.down("xgrid[name='dbssamplesgrid']");
            if (dbs) dbs.loadData();

            if (ts.DBS!=true){
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("dbssample"),container: "dcordbs",disabled: true},obj));
            } else {
                if (dbsfound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("dbssample"),container: "dcordbs",completed: [dbscomplete,ts.DBSSampleBCOSignature_completed,ts.DBSSampleDCOSignature_completed]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate("dbssample"),container: "dcordbs", completed: [false]},obj));
                }
            }

	    //Part 1 (Sample Info)
            if (dbsfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"),container: "DBSSampleInfo", completed: dbscomplete},obj));
            } else {
                  this.setComplete(Ext.apply({title: this.main.localization.translate("sampleinfo"), container: "DBSSampleInfo", completed: false},obj));
            }
	    //Part 2 (BCO Signature)
            if (ts.DBS) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "DBSSampleBCOSignature",managers:["DBSSampleBCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("bcosignaturetitle"),container: "DBSSampleBCOSignature",required: true},obj));
            }
            
            //Part 3 (DCO Signature)
            if (ts.DBS) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DBSSampleDCOSignature",managers:["DBSSampleDCOSignature"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DBSSampleDCOSignature",required: true},obj));
            }

            //Urine Sample Form
            //-------------------------------
            
            //Need to loop over urinesamples.
            var ucomplete = true;
            var ufound = false;
            var usamples = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ urinesample");
            for (var i=0,l=usamples.length;i<l;i++) {
                ufound = true;
                var u = usamples[i];
                if (u.ChaperoneForm_completed && u.urinesampleform_completed) {
                } else {
                    ucomplete = false;
                }
            }
        
            if (ts.Urine!=true) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('urinesample'),container: 'dcorur',disabled: true},obj));
            } else {
                if (ufound) {
                    this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('urinesample'),container: 'dcorur',completed:[ucomplete]},obj));
                } else {
                    this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('urinesample'),container: 'dcorur',completed:[false]},obj));
                }
            }
            
            //Recheck Urine Samples list.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='urinesamples']");
            //if (dvw) dvw.loadData();
            
                  
            //Part 1 (Chaperone)
            if (ufound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("chaperone"),container: "ChaperoneForm",managers:["ChaperoneForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("chaperone"),container: "ChaperoneForm",required: true},obj));
            }
            
            //Part 2 (Urine Sample)
            if (ufound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("urinesample"),container: "urinesampleform",managers:["urinesampleform"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("urinesample"),container: "urinesampleform",required: true},obj));
            }


            //Research Consent Form
            //-------------------------------
	    if (ts.AcceptTerms=="Yes") {
            	this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('researchconsenttitle'),container: 'dcorcs',completed:[ts.ResearchConsentPart1_completed,ts.ResearchConsentPart2_completed]},obj));
	    } else {
            	this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('researchconsenttitle'),container: 'dcorcs',completed:[ts.ResearchConsentPart1_completed]},obj));
	    }	
            
            
            //Part 1 (Accept Terms)
            this.checkSection(Ext.apply({title: this.main.localization.translate("accepttermstitle"),container: "ResearchConsentPart1",managers: ["ResearchConsentPart1"],required: true},obj));
            
            //Part 2 (Athlete Signature)
	    if (ts.AcceptTerms=="Yes") {
            	this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "ResearchConsentPart2",managers: ["ResearchConsentPart2"],required: true},obj));
	    } else {
            	this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "ResearchConsentPart2",required: true},obj));
	    }


            //Declaration of Use Form
            //-------------------------------
            this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('declarationofuse'),container: 'dcordou',completed:[ts.douDeclarations_completed,ts.douAgreement_completed]},obj));
            
            //Part 1 (Declarations)
            this.checkSection(Ext.apply({title: this.main.localization.translate("declarations"),container: "douDeclarations",managers: ["douDeclarations"],required: true},obj));
            
            //Part 2 (Laboratory/Sample Information)
            this.checkSection(Ext.apply({title: this.main.localization.translate("labsampleinfo"),container: "douLabSampleInfo",required: true},obj));
            
            //Part 3 (Agreement)
            this.checkSection(Ext.apply({title: this.main.localization.translate("agreement"),container: "douAgreement",managers: ["douAgreement"],required: true},obj));



            //Partial Sample Form
            //-------------------------------
            
            //Need to loop over partial samples.
            var pscomplete = true;
            var pfound = false;
            var partials = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ partialsample");
            for (var i=0,l=partials.length;i<l;i++) {
                pfound = true;
                var p = partials[i];
                if (p.dcorpsVolumeKit_completed && p.dcorpsDateTimeForm_completed && p.dcorpsWitnessSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDCOSig_completed && p.dcorpsDateTimeOpened_completed && p.dcorpsAthleteSig_completed && p.dcorpsDCOSig2_completed) {
                } else {
                    pscomplete = false;
                }
            }
            
            //Recheck Partial Samples list.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='PartialSampleList']");
            //if (dvw) dvw.loadData();
            
            if (pfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('partialsample'),container: 'dcorpt',completed:[pscomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('partialsample'),container: 'dcorpt'},obj));
            }
            
            
            //Part 1 (Volume/Kit)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("volumekit"),container: "dcorpsVolumeKit",managers: ["dcorpsVolumeKit"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("volumekit"),container: "dcorpsVolumeKit",required: true},obj));
            }
            
            //Part 2 (Date/Time Sealed)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimesealed"),container: "dcorpsDateTimeForm",managers:["dcorpsDateTimeForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimesealed"),container: "dcorpsDateTimeForm",required: true},obj));
            }
            
            //Part 3 (Witness Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title:this.main.localization.translate( "witnesssignature"),container: "dcorpsWitnessSig",managers: ["dcorpsWitnessSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("witnesssignature"),container: "dcorpsWitnessSig",required: true},obj));
            }
            
            //Part 4 (DCO Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig",managers: ["dcorpsDCOSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig",required: true},obj));
            }
            
            //Part 5 (Date/Time Opened)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimeopened"),container: "dcorpsDateTimeOpened",managers: ["dcorpsDateTimeOpened"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("datetimeopened"),container: "dcorpsDateTimeOpened",required: true},obj));
            }
            
            //Part 6 (Athlete Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "dcorpsAthleteSig",managers: ["dcorpsAthleteSig"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "dcorpsAthleteSig",required: true},obj));
            }
            
            //Part 7 (DCO Signature)
            if (pfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig2",managers: ["dcorpsDCOSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "dcorpsDCOSig2",required: true},obj));
            }


            //Custody Transfer Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var ctcomplete = true;
            var cfound = false;
            var transfers = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ custodytransfer");
            for (var i=0,l=transfers.length;i<l;i++) {
                cfound = true;
                var ct = transfers[i];
                if (ct.reasonForm_completed && ct.AthleteSig2_completed && ct.DcoSigForm_completed  && ct.TransferAthleteForm_completed && ct.AthleteSigForm_completed && ct.DcoSig2_completed) {
                } else {
                    ctcomplete = false;
                }
            }
            
            //Recheck Custody Transfer form.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='custodytransferlist']");
            //if (dvw) dvw.loadData();
            
            if (cfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('custodytransfertitle'),container: 'dcortrans',completed:[ctcomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title:this.main.localization.translate('custodytransfertitle'),container: 'dcortrans'},obj));
            }
                  
            //Part 1 (Reason for Transfer)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("reasonfortransfer"),container: "reasonForm",managers: ["reasonForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("reasonfortransfer"),container: "reasonForm",required: true},obj));
            }
            
            //Part 2 (Athlete Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSig2",managers:["AthleteSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSig2",required: true},obj));
            }
            
            //Part 3 (DCO Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSigForm",managers: ["DcoSigForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSigForm",required: true},obj));
            }
            
            //Part 4 (Transfer to Athlete)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("transfertoathlete"),container: "TransferAthleteForm",managers: ["TransferAthleteForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("transfertoathlete"),container: "TransferAthleteForm",required: true},obj));
            }
            
            //Part 5 (Athlete Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSigForm",managers: ["AthleteSigForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "AthleteSigForm",required: true},obj));
            }
            
            //Part 6 (DCO Signature)
            if (cfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSig2",managers: ["DcoSig2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "DcoSig2",required: true},obj));
            }
            


            //Supplementary Form
            //-------------------------------
            
            //Need to loop over custody transfers.
            var supcomplete = true;
            var sfound = false;
            var supplementaries = this.main.data.filter(this.main.data.getChildren(ts),"cid EQ supplementaryreport");
            for (var i=0,l=supplementaries.length;i<l;i++) {
                sfound = true;
                var sup = supplementaries[i];
		// Each supplementary report can have multiple signatures
		// Loop through these to make sure all are complete
		//var supsigs = this.main.data.filter(this.main.data.getChildren(sup),"cid EQ supportperson");
		//var ssigcomplete = true;
		//var ssigfound = false;
		//for (var j=0,k=supsigs.length;j<k;j++) {
		//	ssigfound = true;
		//	var supsig = supsigs[j];
		//	if (supsig.dcorsupPart5_completed) {
		//	} else {
		//		ssigcomplete = false;
		//	}
		//}
		//if (ssigfound) {
			// save status of signatures to the supplementary report
		//	this.main.actions.perform({
		//		cmd: "put",
		//		component: this,
		//		objects:[sup],
		//		dcorsupPart5_completed: ssigcomplete,
		//		dcorsupPart5_started: ssigfound
		//	});
		//}

                if (sup.dcorsupPart2_completed) {
                } else {
                    supcomplete = false;
                }
            }

	    //var ssp = this.main.data.getProvider("{supplementarysupportperson}");
            
            //Recheck Supplementary form.
            //Might need to move this into a rule that only works when data is being added.
            //var dvw = sp.component.down("xgrid[name='addsupplementarylist']");
            //if (dvw) dvw.loadData();
            

            if (sfound) {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('supplementary'),container: 'dcorsup',completed:[supcomplete]},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('supplementary'),container: 'dcorsup'},obj));
            }
            
                  
            //Part 1 (Applies To)
            this.checkSection(Ext.apply({title: this.main.localization.translate("appliesto"),container: "dcorsupPart1",required: true},obj));
            
            //Part 2 (Person Submitting)
            if (sfound) {
                this.checkSection(Ext.apply({title: this.main.localization.translate("personsubmittingtitle"),container: "dcorsupPart2",managers:["dcorsupPart2"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("personsubmittingtitle"),container: "dcorsupPart2",required: true},obj));
            }
            
            //Part 3 (Purpose of Report)
            this.checkSection(Ext.apply({title: this.main.localization.translate("purposeofreport"),container: "dcorsupPart3",required: true},obj));
            
            //Part 4 (Comments)
            this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "dcorsupPart4",required: true},obj));
            
            //Part 5 (Signature)
            //if (sfound) {
            //    this.checkSection(Ext.apply({title: this.main.localization.translate("signature"),container: "dcorsupPart5",managers: ["dcorsupPart5"],required: true},obj));
            //} else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("signature"),container: "dcorsupPart5",required: true},obj));
            //}
            
            
            var ath = this.main.data.getProvider("{activetestsession > athlete[0]}");
           

            //Signatures Form
            //-------------------------------
            
            this.checkForm(Ext.apply({dataprovider:ts,title: this.main.localization.translate('signatures'),container: 'dcorsig',completed:[ts.signaturesPart2_completed,ts.signaturesPart7_completed]},obj));
           
            // The representative signature form has been rolled up into the other signatures form      
            //Part 1 (Representative)
            //this.checkSection(Ext.apply({title: this.main.localization.translate("athleterepresentative"),container: "representativesignatureform"},obj));
            
            //Part 2 (Other Signatures)
            this.checkSection(Ext.apply({title: this.main.localization.translate("othersignatures"),container: "signaturesPart5"},obj));
            
            //Part 3 (Subsequent DCO)
            this.checkSection(Ext.apply({title: this.main.localization.translate("subsequentofficer"),container: "signaturesPart6"},obj));

            //Part 4 (Review)
            this.checkSection(Ext.apply({title: this.main.localization.translate("reviewforms"), container: "signaturesPart1", required:true},obj));
            
            //Part 5 (DCO Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("dcosignature"),container: "signaturesPart7", managers: ["signaturesPart7"],required:true},obj));

            //Part 6 (Athlete Signature)
            this.checkSection(Ext.apply({title: this.main.localization.translate("athletesignature"),container: "signaturesPart2", managers: ["signaturesPart2"],required:true},obj));
                  
            //Part 8 (Receipt)
            this.checkSection(Ext.apply({title: this.main.localization.translate("receipt"),container: "signaturesPart8"},obj));
            
            //Part 9 (Submit Forms)
            this.checkSection(Ext.apply({title: this.main.localization.translate("submit"),container: "signaturesPart9"},obj));
            
            this.checkCollection(Ext.apply({button:"submittestsessionbutton",completed:[ts.dcornot_completed,ts.dcorai_completed,ts.dcorlab_completed,ts.dcorbl_completed,ts.dcorur_completed,ts.dcorcs_completed,ts.dcordou_completed,ts.dcorpt_completed,ts.dcortrans_completed,ts.dcorsup_completed,ts.dcorsig_completed],override:ts.papertest},obj));
            
            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    
     checkuarHandler: function (sp) {
    
        //This function is called after rule's component (UAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the UAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var uar = this.main.data.getProvider("{activeuar}");
            var loc = this.main.data.getProvider("{activeuar > location}");

            
            //Athlete Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate('athleteinfotitle'),container:'uarathleteinfoform',completed:[uar.uarathleteinfoform_completed]},obj));
            

            //Attempt Information
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate('attemptinformation'),container:'attemptinfoform',completed:[uar.attemptinfoform_completed]},obj));
            
            //First Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var pcomplete = true;
            var pfound = false;
            if (uar.FirstCallsPlaced) {
                var pnumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=pnumbers.length;i<l;i++) {
                    var p = pnumbers[i];
                    if (p.UARLocation=='FirstLocation'){
                    	pfound=true;
                        if (p.FirstLocNumbersCalledForm_completed) {
                        } else {
                            pcomplete = false;
                        }
                    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            //var dvw = sp.component.down("xgrid[name='uarfirstnumberscalledgrid']");
            //if (dvw) dvw.loadData();
            
            
            this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate("firstlocation"),container: "FirstLocationTabPanel",completed: [pcomplete,uar.FirstLocationLocationForm_completed,uar.FirstLocationTimeForm_completed,uar.FirstLocationCommentsForm_completed]},obj));
            
            //Location
            this.checkSection(Ext.apply({title: this.main.localization.translate("firstlocation"),container: "FirstLocationLocationForm",managers:["FirstLocationLocationForm"],required: true},obj));
            
            //Time
            this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "FirstLocationTimeForm",managers:["FirstLocationTimeForm"],required: true},obj));
            
            //Person Contacted
            this.checkSection(Ext.apply({title: this.main.localization.translate("personcontacted"),container: "FirstLocationPersonContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: this.main.localization.translate("restrictedaccess"),container: "FirstLocationRestrictedAccessForm",required: true},obj));
            
            //Numbers Called
            if (pfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "FirstLocNumbersCalledForm",completed: pcomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "FirstLocNumbersCalledForm",required: true},obj));
            }
            
            
            //Comments
            this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "FirstLocationCommentsForm",managers:["FirstLocationCommentsForm"],required: true},obj));
            
            
            
            //Second Location
            //-------------------------------
            
            //Need to loop over Phone Calls.
            var scomplete = true;
            var sfound = false;
            if (uar.SecondCallsPlaced==true) {
                var snumbers = this.main.data.filter(this.main.data.getChildren(uar),"cid EQ phonecall");
                for (var i=0,l=snumbers.length;i<l;i++) {
                    var sn = snumbers[i];
		    if (sn.UARLocation=='SecondLocation'){
                    	sfound = true;
                    	if (sn.SecondLocNumbersCalledForm_completed) {
                    	} else {
                    	    scomplete = false;
                    	}
		    }
                }
            }
            
            //Recheck Phone Calls list.
            //Might need to move this into a rule that only works when Blood data is being added.
            //var dvw = sp.component.down("xgrid[name='uarsecondnumberscalledgrid']");
            //if (dvw) dvw.loadData();
            
            //secondlocationvisited
            if (uar.secondlocationvisited=="Yes") {
                this.checkForm(Ext.apply({dataprovider:uar,title: this.main.localization.translate("secondlocation"),container: "SecondLocationTabPanel",completed: [scomplete,uar.SecondLocationLocationForm_completed,uar.SecondLocationTimeForm_completed,uar.SecondLocationCommentsForm_completed]},obj));
            } else if (uar.secondlocationvisited=="No") {
                this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('secondlocation'),container:'SecondLocationTabPanel'},obj));
            } else {
                this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('secondlocation'),container:'SecondLocationTabPanel',completed: [uar.SecondLocationLocationForm]},obj));
            }
            
            //Location
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("location"),container: "SecondLocationLocationForm",managers:["SecondLocationLocationForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("location"),container: "SecondLocationLocationForm",required: true},obj));
            }
            
            //Time
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "SecondLocationTimeForm",managers:["SecondLocationTimeForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("time"),container: "SecondLocationTimeForm",required: true},obj));
            }

            
            //Person Contacted
            this.checkSection(Ext.apply({title: this.main.localization.translate("personcontacted"),container: "SecondLocationContactedForm",required: true},obj));
            
            //Restricted Access
            this.checkSection(Ext.apply({title: this.main.localization.translate("restrictedaccess"),container: "SecondLocationRestrictedAccessForm",required: true},obj));
            
            //Numbers Called
            if (sfound) {
                this.setComplete(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "SecondLocNumbersCalledForm",completed: scomplete},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("phonenumberscalled"),container: "SecondLocNumbersCalledForm",required: true},obj));
            }

            
            //Comments
            if (uar.secondlocationvisited=="Yes") {
                this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "SecondLocationCommentsForm",managers:["SecondLocationCommentsForm"],required: true},obj));
            } else {
                this.checkSection(Ext.apply({title: this.main.localization.translate("comments"),container: "SecondLocationCommentsForm",required: true},obj));
            }
            
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('comments'),container:'uarcommentsform'},obj));
            
            //DCO Confirmation
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:uar,title:this.main.localization.translate('dcoconfirmation'),container:'dcoconfirmationform',completed:[uar.dcoconfirmationform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submituarbtn',completed:[uar.uarathleteinfoform_completed,uar.attemptinfoform_completed,uar.FirstLocationTabPanel_completed,uar.SecondLocationTabPanel_completed,uar.dcoconfirmationform_completed]},obj));
            

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
     checkaarHandler: function (sp) {
    
        //This function is called after rule's component (AAR tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the AAR section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var aar = this.main.data.getProvider("{activeaar}");
            
            //Location of Completed Test
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('locationcompletedtest'),container:'locationofcompletedtestform',completed:[aar.locationofcompletedtestform_completed]},obj));
            

            //Other Locations
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('otherlocations'),container:'otherlocationsform'},obj));
            
            
            //Notification
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('notification'),container:'aarnotificationform',completed:[aar.aarnotificationform_completed]},obj));
            
            //Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('processing'),container:'aarprocessingform',completed:[aar.aarprocessingform_completed]},obj));
            
            //Post Processing
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('postprocessing'),container:'aarpostprocessingform',completed:[aar.aarpostprocessingform_completed]},obj));
            
            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('comments'),container:'aarcommentsform'},obj));
            
            //Signature
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:aar,title:this.main.localization.translate('signature'),container:'aarsignatureform',completed:[aar.aarsignatureform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'submitaarbutton',completed:[aar.locationofcompletedtestform_completed,aar.aarnotificationform_completed,aar.aarprocessingform_completed,aar.aarpostprocessingform_completed,aar.aarsignatureform_completed]},obj));

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },

     checkmanifestHandler: function (sp) {
    
        //This function is called after rule's component (Sample Manifest tabpanel) is added to the screen.
        //It is also called every time the completed context changes from any of the managers in the Sample Manifest section.
        
        this.main.actions.perform({
            cmd: 'setbusy',
            component: sp.component,
            message: 'Checking progress...'
        });
        
        //Important: Defer to allow all components to draw on screen, then must set up all managers in
        //manageProviders before this function will work correctly.
        
        /* Common pitfalls so far:
            incorrect provider
            provider must be character exact
            must separate providers and managers into trees so they do not overlap.
            provider name is case sensitive and must be exact
            forgetting to put required:true on a component
            
        */

        Ext.defer(function () {
        
            var obj = {
                component: sp.component,
                subtopic: (sp.rule) ? sp.rule.value : ''
            }
            
            //container = name of the component you want to mark with complete/incomplete style.
            //manager = name of a manager component that is managing a section of fields and marking them complete/incomplete
            //title = base title to alter with complete/incomplete style.
            //required = if true it will ignore started setting and track the completeness of that section no matter what.
            
            var am = this.main.data.getProvider("{activemanifest}");
            
            //Urine Samples
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('urinesamples2'),container:'selectusamplesform'},obj));
            

            //Blood Samples
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('bloodsamples'),container:'selectbsamplesform'},obj));
            
            //DBS Samples
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('dbssamples'),container:'selectdbssamplesform'},obj));

            //Date/Time
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('datetime'),container:'smdatetimeform',completed:[am.smdatetimeform_completed]},obj));
            
            //Urine Analysis
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('urineanalysis'),container:'urineanalysisform'},obj));
            
            //Blood Analysis
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('bloodanalysis'),container:'bloodanalysisform'},obj));
            
	    //DBS Analysis
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('dbsanalysis'),container:'dbsanalysisform'},obj));

            //Shipping Info
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('shippinginfo'),container:'shippinginfoform',completed:[am.shippinginfoform_completed]},obj));

             //Waybill
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('waybill'),container:'waybillpanel'},obj));

            //Comments
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('comments'),container:'smcommentsform'},obj));
            
            //Signature
            //--------------------------------
            this.checkForm(Ext.apply({dataprovider:am,title:this.main.localization.translate('dcosignature'),container:'smdcosignatureform',completed:[am.smdcosignatureform_completed]},obj));
            
            //Check completed status on all forms
            this.checkCollection(Ext.apply({button:'savemanifestbutton',completed:[am.smdcosignatureform_completed,am.smdatetimeform_completed,am.shippinginfoform_completed]},obj));

            //Done
            this.main.actions.perform({
                cmd: 'removebusy',
                component: sp.component
            });
            
        },1000,this);

    },
    
    
    checkSection: function (sp) {
    
        //Check a section for completeness and set style.
    
        try {
        
            var managers = sp.managers;
            var started = false;
            var completed = true;
            
            if (Ext.isArray(managers)) {
            
                //Combine all the different managers to create a complete/started condition for all managers.
                for (var i=0,l=managers.length;i<l;i++) {
                    var n = managers[i];
                    var manager = this.main.data.getObjectByName(n);
                    var provider = this.main.data.getProvider(manager.manage);
                    if (provider) {
                        started = started||(provider[n+"_started"]);
                        completed = completed&&(provider[n+"_completed"]);
                    } else {
                        console.log("couldn't find provider " + manager.manage);
                    }
                }

                var container = this.main.data.getObjectByName(sp.container);
                if (started||sp.required) {
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    if (completed) {
                        o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                    } else {
                        o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                    }
                }
               
            } else {
           
                //Enable this section if you want to have all tabs with checkmarks even if there are no required fields.
                //Leave this section disabled if you want to only put checkmarks on tabs with required information.
           
           
                try {
                    //Manual override to give green checks to sections that don't have any required fields.
                    var container = this.main.data.getObjectByName(sp.container);
                    var selector = container.cid + "[name='" + sp.container + "']";
                    var o = sp.component.down(selector);
                    o.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
                } catch (e) {
                    console.log("Couldn't locate " + selector);
                }
           
            }
        
        } catch (e) {
            //debugger;
        }
        
    },
    
    setComplete: function (sp) {
        //Manual switch for special cases.
        var container = this.main.data.getObjectByName(sp.container);
        var selector = container.cid + "[name='" + sp.container + "']";
        var o = sp.component.down(selector);
        if (sp.completed) {
            o.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
        } else {
            o.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
        }
    
    },
    
    checkForm: function (sp) {
        //Manual check for main forms.
        var notreq = false;
        if (!sp.completed) {
            sp.completed = [true];
            notreq = true;
        }
        if (!sp.disabled) sp.disabled = false;
            
        var v = sp.completed.every(function(i){
            return i;
        });
            
        var frm = sp.component.down("xtabpanel[name='" + sp.container + "']")||sp.component.down("xform[name='" + sp.container + "']")||sp.component.down("xpanel[name='" + sp.container + "']");
            
        var ap = {
            cmd: "put",
            component: this,
            objects: [sp.dataprovider]
        };
        ap[sp.container+"_completed"] = v;
        this.main.actions.perform(ap);
        
        if (frm) {
            if (sp.disabled||notreq) {
                frm.setTitle('<span class="fa fa-circle" style="color:grey;"></span> ' + sp.title);
            } else {
                if (v) {
                    frm.setTitle('<span class="fa fa-check-circle" style="color:green;"></span> ' + sp.title);
                } else {
                    frm.setTitle('<span class="fa fa-ban" style="color:red;"></span> ' + sp.title);
                }
            }
        }
        
    },
    
    checkCollection: function (sp) {
        if (!sp.completed) sp.completed = [true];
        
        var x = (sp.override===true) ? true : false ;
        
        var v = sp.completed.every(function(i){
            return i;
        });
        
        var btn = sp.component.down("xbutton[name='" + sp.button + "']");
           
        if (btn) {
            if (v || x) {
                btn.enable(true);
            } else {
                btn.disable(true);
            }
        }
    },

    /*----------------------------------------------------------------
     * Utility functions
     * These functions are needed for the web version and are used to
     * repair problems with test submission, xml creation, etc...
     * -------------------------------------------------------------*/

    
   rebuildlabxmlHandler: function () {
       var cmp = this.main.view.down("*[name='rebuildsample']");
       var samplealias = cmp.getValue();
       if (samplealias) {
           this.main.actions.perform({
               cmd: 'request',
               a: samplealias,
               relationship: 'descendants',
               scope: this,
               callback: this.rebuildlabxmltsCallback
           });
       }
   },


   rebuildlabxmltsCallback: function (rsp) {
       var me = (rsp.scope||this);

       var tsalias = rsp.leaf.pa;
       if (tsalias) {
           me.main.actions.perform({
               cmd: 'request',
               a: tsalias,
               relationship: 'descendants',
               scope: me,
               callback: me.rebuildlabxmlCallback
           });
       } else {
           me.main.actions.perform({
               cmd: 'say',
               component: me,
               title: 'Problem',
               message: 'Cant rebuild.'
           });
       }
   },



   rebuildlabxmlCallback: function (rsp) {
       var me = (rsp.scope||this);

       var cmp = me.main.view.down("*[name='rebuildsample']");
       var samplealias = cmp.getValue();

       var objTpl = me.main.data.getObjectByName('labxmltemplate');
       var tpl = new Ext.XTemplate(objTpl.ct,me.main.util.getTemplateFunctions());

       if (samplealias) {
           Ext.defer(function () {
               var h = tpl.apply(me.main.data.map[samplealias]);
               me.main.actions.perform({
                   cmd: 'put',
                   component: me,
                   objects: [me.main.data.getObject(samplealias)],
                   ct: h
               });
               me.main.actions.perform({
                   cmd: 'say',
                   component: me,
                   title: 'Lab XML Saved',
                   message: h
               });
           },10000,this);
       } else {
           me.main.actions.perform({
               cmd: 'say',
               component: me,
               title: 'Error',
               message: 'Cant find this sample.'
           });
       }
   },


   rebuildtestpacketHandler: function () {
       var cmp = this.main.view.down("*[name='rebuildtestpacket']");
       var tsalias = cmp.getValue();
       if (tsalias) {

           this.main.actions.perform({
               cmd: 'request',
               a: tsalias+'.testpacket',
               relationship: 'item',
               scope: this,
               callback: this.loadtestpacketCallback
           });

       }
   },

   loadtestpacketCallback: function (rsp) {
       var me = (rsp.scope||this);
       var cmp = me.main.view.down("*[name='rebuildtestpacket']");
       var tsalias = cmp.getValue();
       me.main.actions.perform({
           cmd: 'request',
           a: tsalias,
           relationship: 'descendants',
           scope: me,
           callback: me.rebuildtestpacketCallback
       });
   },

   rebuildtestpacketCallback: function (rsp) {
       var me = (rsp.scope||this);

       var cmp = me.main.view.down("*[name='rebuildtestpacket']");
       var tsalias = cmp.getValue();
       var ts = me.main.data.getObject(tsalias);

       if (ts) {

           var tarray = [];
           var earray = [];
           var upass = false, spass = false, apass = false;

           if (ts.UAR_completed) {
               upass = true;
           } else {
               if (ts.SampleShipment_completed) {
                   spass = true;
               } else {
                   earray.push("All samples for " + ts.FirstName + ' ' + ts.LastName + " test session have not been shipped.");
               }
               if (ts.designation=='event' || ts.AfterAction_completed) {
                   apass = true;
               } else {
                   earray.push("After action report for " + ts.FirstName + ' ' + ts.LastName + " test session has not been completed.");
               }
           }

           if (upass || (spass && apass)) tarray.push(ts);

           if (earray.length > 0) {
               var elist = earray.join('<br>');
               me.main.feedback.say({
                   title: 'Submission Cancelled',
                   message: 'Some of the selected tests could not be submitted:<br>' + elist
               });
               return;
           }

           if (tarray.length==0) {
               me.main.feedback.say({
                   title: 'Error',
                   message: 'You must select at least one test session to submit.'
               });
               return;
           }
           for (var i=0,l=tarray.length;i<l;i++) {
               // Get the raw objects for test session and docs
               // otherwise json encode will fail
               var arr = [];
               var desc = me.main.data.getDescendants(tarray[i].a);
               for (index in desc) {
                   var o = me.main.data.getObject(desc[index].a);
                   if ((o.cid=='declaration')&&(o.t==1)) {
                       //Do not include declarations that are linked to athlete object.
                   } else {
                       arr.push(o);
                   }
               }
               try {
                   var s = Ext.JSON.encode(arr);
               } catch(err) {
                   me.main.feedback.say({
                       title: 'Error creating test packet!',
                       message: err
                   });
                   return;
               }

               var packet = me.main.data.getObject(tarray[i].a+".testpacket");

               // update the contents of this test packet.  But delay it to give a chance for the data
               // read in the first step to save to the database before continuing.
               Ext.defer(function () {
                   me.main.data.put({
                       component: me,
                       scope: me,
                       objects: [packet],
                       ct: s
                   });
               },10000,this);

           }

           me.main.feedback.say({
               title: 'Info',
               message: 'Test Packet Rebuilt'
           });

       } else {
           me.main.actions.perform({
               cmd: 'say',
               component: me,
               title: 'Error',
               message: 'Cant find this test session.'
           });
       }
   },


   rebuildDCFHandler: function () {
       var cmp = this.main.view.down("*[name='rebuilddcf']");
       var tsalias = cmp.getValue();
       if (tsalias) {

           this.main.actions.perform({
               cmd: 'request',
               a: tsalias,
               relationship: 'descendants',
               scope: this,
               callback: this.rebuildDCFCallback
           });

       }
   },



   rebuildDCFCallback: function (rsp) {
       var me = (rsp.scope||this);

       debugger;

       var apn='';
       var testsession = rsp.leaf;
       var dcor = me.main.data.filter(rsp.desc,'cid EQ document AND doctype EQ dcor')[0];
       var report = this.main.data.map[testsession.a];
       var athlete = me.main.data.filter(rsp.desc,'cid EQ athlete')[0];
       var apnfl = apn.concat(me.main.data.getDerivedValue(athlete,'_FirstName'),', ',me.main.data.getDerivedValue(athlete,'_LastName'));
       var apncmb = apn.concat(me.main.data.getDerivedValue(athlete,'_FirstName'),' ',me.main.data.getDerivedValue(athlete,'_LastName'));

       var ts = new Date();
       var ds = Ext.Date.format(ts,'m-d-Y h:i A');

       var tpl = new Ext.XTemplate(me.main.data.getObjectByName('dcfReview').ct,me.main.util.getTemplateFunctions());
       var output = tpl.apply(report);
       output = output.concat(me.main.localization.translate('languagedisclaimer'), ds, "</div>");

       var combinedoutput = output;
       // If athlete is UFC or Professional Boxing create the dcor receipt
       if (athlete.Sport=='155'||athlete.Sport=='139') {
           var receipttpl = new Ext.XTemplate(me.main.data.getObjectByName('dcorReceipt').ct,me.main.util.getTemplateFunctions());
           var receipt = receipttpl.apply(report);
           combinedoutput = output.concat(receipt);
       }



       //Save the updated pdf.
       Ext.defer(function () {
           me.main.data.put({
               component: me,
               scope: me,
               objects: [dcor],
               ct: combinedoutput,
               DocumentGenerated: ds
           });

           //Email it.
           me.main.data.task({
               method: "swiftemail",
               toaddress: me.main.environment.get("erroraddress"),
               data: combinedoutput,
               subject: "Resubmitted DCF",
               headertext: apncmb + " Page &p; / &P;",
               footertext: "Copyright United States Anti-Doping Agency.  REV 4/2017",
               headeralign: 'right',
               filename: 'dcor_' + ds + '.pdf',
               css: "http://paperlessbravo.usada.org/resources/pdf.css",
               base: "http://paperlessbravo.usada.org/"
           });

       },10000,me);

       me.main.feedback.say({
           title: 'Info',
           message: 'DCF Rebuilt and Emailed'
       });

    },
           
        
        
        
});

Ext.ClassManager.addNameAliasMappings({
    'Practice.view.main.Actions': ['custom']
});
